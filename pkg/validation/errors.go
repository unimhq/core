package validation

import (
	"fmt"
	"strings"
	"time"
)

// common errors
const (
	NotEmptyError = "can't be empty"
	NotNilError   = "can't be null"
	NotZeroError  = "can't be zero"
)

func OneOf(availableValues []string) string {
	return "Value must be one of " + strings.Join(availableValues, ",")
}

func BeforeError(before time.Time) string {
	return fmt.Sprintf("must be before %s", before)
}

func MinError(min uint64) string {
	return fmt.Sprintf("must be greater than %d", min)
}

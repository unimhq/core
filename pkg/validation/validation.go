package validation

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/pkg/errors"
)

// todo(egor): integrate with `https://github.com/go-playground/validator`

type ValidationError struct {
	FieldName string                 `json:"fieldName"`
	Message   string                 `json:"message"`
	Details   map[string]interface{} `json:"details"`
}

type ValidationResult struct {
	Errors []ValidationError
}

type ValidationResultBuilder struct {
	Errors []ValidationError
}

func (builder ValidationResultBuilder) Add(fieldName string, message string) ValidationResultBuilder {
	ve := ValidationError{
		FieldName: fieldName,
		Message:   message,
		Details:   make(map[string]interface{}),
	}

	return ValidationResultBuilder{
		Errors: append(builder.Errors, ve),
	}
}

func (builder ValidationResultBuilder) Build() ValidationResult {
	return ValidationResult{
		Errors: builder.Errors,
	}
}

func (vr ValidationResult) IsValid() bool {
	return len(vr.Errors) == 0
}

func (vr ValidationResult) ToError() error {
	errorsAsJson, err := json.Marshal(vr.Errors)
	if err != nil {
		return errors.Wrap(err, fmt.Sprintf("Can' serialize to json validation errors %v", vr))
	}

	return errors.New(fmt.Sprintf("Not valid: `%s`", string(errorsAsJson)))
}

type SelfValidated interface {
	Validate(ctx context.Context) ValidationResult
}

func Ok() ValidationResultBuilder {
	return ValidationResultBuilder{}
}

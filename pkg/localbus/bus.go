package localbus

import (
	"context"
	"fmt"
	"github.com/pkg/errors"
	"go.uber.org/zap"
	"reflect"
)

type bus struct {
	syncEventListeners        map[Topic][]reflect.Value
	asyncEventListeners       map[Topic][]reflect.Value
	warningOnEmptySubscribers map[Topic]bool
}

func (b *bus) Publish(ctx context.Context, event Event) error {
	topic := event.GetTopicName()
	slogger().Infof("Event occurred topic: %s, event: %+v", topic, event)

	syncEventListeners := b.getSyncEventListener(topic)
	asyncEventListeners := b.getAsyncEventListener(topic)

	// warning about empty subscribers
	if len(syncEventListeners)+len(asyncEventListeners) == 0 {
		if warningOnEmptySubscribers, ok := b.warningOnEmptySubscribers[event.GetTopicName()]; !ok || warningOnEmptySubscribers {
			slogger().Warnf("Empty subscribers for topic %s, event = %+v", topic, event)
		}
	}

	// run sync handlers one-by-one
	syncArguments := []reflect.Value{reflect.ValueOf(ctx), reflect.ValueOf(event)}
	asyncArguments := []reflect.Value{reflect.ValueOf(event)}

	for _, syncEventListener := range syncEventListeners {
		result := syncEventListener.Call(syncArguments)
		err := extractError(result)
		if err != nil {
			return errors.Wrap(err, fmt.Sprintf("Can't perform event handling for topic: %s, event: %+v", topic, event))
		}
	}

	// run async handlers one-by-one
	go func() {
		defer func() {
			if err := recover(); err != nil {
				slogger().Errorw("Async event listener failed", "error", err)
			}
		}()

		for _, asyncEventListener := range asyncEventListeners {
			result := asyncEventListener.Call(asyncArguments)
			err := extractError(result)
			if err != nil {
				zap.S().Error("Can't perform event handling for topic: %s, event: %+v", topic, event)
			}
		}
	}()

	return nil
}

func getErrorInterface() reflect.Type {
	return reflect.TypeOf((*error)(nil)).Elem()
}

func extractError(results []reflect.Value) error {
	result := results[0]

	if !result.Type().Implements(getErrorInterface()) {
		return errors.New(fmt.Sprintf("Wrong function signature for result %v", results))
	}

	if !result.IsNil() {
		return errors.New(fmt.Sprintf("Error: %+v", result.Interface()))
	}

	return nil
}

func (b *bus) Subscribe(topic Topic, eventListener interface{}) {
	reflectType := reflect.TypeOf(eventListener)
	if !(reflectType.Kind() == reflect.Func) {
		panic(errors.New(fmt.Sprintf("%s is not of type reflect.Func", reflect.TypeOf(eventListener).Kind())))
	}

	if reflectType.NumIn() != 2 || reflectType.NumOut() != 1 {
		panic(errors.New(fmt.Sprintf("%s is not correct signature reflect.Func", reflect.TypeOf(eventListener).Kind())))
	}

	resultType := reflectType.Out(0)

	if !resultType.Implements(getErrorInterface()) {
		panic(errors.New(fmt.Sprintf("%s is not correct signature reflect.Func ", reflect.TypeOf(eventListener).Kind())))
	}

	// todo(egor): check function signature - In

	newListeners := append(b.getSyncEventListener(topic), reflect.ValueOf(eventListener))
	b.syncEventListeners[topic] = newListeners
}

func (b *bus) SubscribeAsync(topic Topic, eventListener interface{}) {
	reflectType := reflect.TypeOf(eventListener)
	if !(reflectType.Kind() == reflect.Func) {
		panic(errors.New(fmt.Sprintf("%s is not of type reflect.Func", reflect.TypeOf(eventListener).Kind())))
	}

	if reflectType.NumIn() != 1 || reflectType.NumOut() != 1 {
		panic(errors.New(fmt.Sprintf("%s is not correct reflect.Func signature", reflect.TypeOf(eventListener).Kind())))
	}

	resultType := reflectType.Out(0)

	if !resultType.Implements(getErrorInterface()) {
		panic(errors.New(fmt.Sprintf("%s is not correct signature reflect.Func ", reflect.TypeOf(eventListener).Kind())))
	}

	// todo(egor): check function signature - In

	newListeners := append(b.getAsyncEventListener(topic), reflect.ValueOf(eventListener))
	b.asyncEventListeners[topic] = newListeners
}

func (b *bus) getSyncEventListener(topic Topic) []reflect.Value {
	topicEventListeners, ok := b.syncEventListeners[topic]
	if !ok {
		topicEventListeners = make([]reflect.Value, 0)
	}

	return topicEventListeners
}

func (b *bus) getAsyncEventListener(topic Topic) []reflect.Value {
	topicEventListeners, ok := b.asyncEventListeners[topic]
	if !ok {
		topicEventListeners = make([]reflect.Value, 0)
	}

	return topicEventListeners
}

func (b *bus) DisableWarningOnEmptySubscribers(topic Topic) {
	b.warningOnEmptySubscribers[topic] = false
}

func NewBus() (Bus, error) {
	return &bus{
		syncEventListeners:        make(map[Topic][]reflect.Value),
		asyncEventListeners:       make(map[Topic][]reflect.Value),
		warningOnEmptySubscribers: make(map[Topic]bool),
	}, nil
}

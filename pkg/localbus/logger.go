package localbus

import "go.uber.org/zap"

func slogger() *zap.SugaredLogger {
	return zap.S().Named("localbus")
}

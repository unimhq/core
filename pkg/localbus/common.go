package localbus

import "context"

type Topic string

type Event interface {
	GetTopicName() Topic
}

type Bus interface {
	/*
		context used for propagation env for sync listeners
	*/
	Publish(ctx context.Context, event Event) error

	Subscribe(topic Topic, eventListener interface{})
	SubscribeAsync(topic Topic, asyncEventListener interface{})

	DisableWarningOnEmptySubscribers(topic Topic)
}

type EventListener func(ctx context.Context, event interface{}) error
type AsyncEventListener func(event interface{}) error

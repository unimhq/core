package config

import (
	"fmt"
	"github.com/pkg/errors"
	"github.com/spf13/viper"
	"io"
	"os"
)

const DefaultDeployEnv = "DEV"
const DefaultAppInstance = "1"
const DefaultConfigPath = "./override-config.yml"

func ReadConfig(appName string, defaultConfigReader io.Reader) (*viper.Viper, *DeployInfo, error) {
	deployInfo, err := getDeployInfo(appName)
	if err != nil {
		return nil, nil, errors.Wrap(err, "Can't read deploy info")
	}

	fmt.Println(fmt.Sprintf("INFO: read config for %+v", *deployInfo))

	config := viper.New()
	config.AutomaticEnv()

	config.SetConfigFile(deployInfo.AppConfigName)
	err = applyDefaultConfig(*deployInfo, config, defaultConfigReader)
	if err != nil {
		return nil, nil, errors.Wrap(err, "Can't apply default config")
	}

	if _, err := os.Stat(deployInfo.AppConfigName); os.IsNotExist(err) {
		fmt.Println(fmt.Sprintf("WARNING: Can't find config path %q, so skip read", deployInfo.AppConfigName))
		return config, deployInfo, nil
	}

	err = config.ReadInConfig()

	if err != nil {
		return nil, nil, errors.Wrap(err, "can't read config")
	}

	return config, deployInfo, nil
}

func applyDefaultConfig(
	deployInfo DeployInfo,
	config *viper.Viper,
	defaultConfigReader io.Reader,
) error {
	if deployInfo.DeployEnv != DefaultDeployEnv {
		fmt.Println(fmt.Sprintf("WARNING: skip apply default config for env %s", deployInfo.DeployEnv))
		return nil
	}

	fmt.Println(fmt.Sprintf("INFO: apply default config"))

	if defaultConfigReader != nil { // should be after `SetConfigFile`, for correct config type detection (yml)
		err := config.MergeConfig(defaultConfigReader)
		if err != nil {
			return errors.Wrap(err, "Can't merge default config")
		}
	}

	return nil
}

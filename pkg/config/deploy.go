package config

import "os"

type DeployInfo struct {
	DeployEnv     string `json:"deployEnv"` // dev, prod, stage,....
	AppName       string `json:"appName"`
	AppInstance   string `json:"appInstance"`
	AppConfigName string `json:"appConfigName"`
}

func getDeployInfo(staticAppName string) (*DeployInfo, error) {
	deployEnv, ok := os.LookupEnv("UNIM_DEPLOY_ENV")
	if !ok {
		deployEnv = DefaultDeployEnv
	}

	appName, ok := os.LookupEnv("UNIM_APP_NAME")
	if !ok {
		appName = staticAppName
	}

	appInstance, ok := os.LookupEnv("UNIM_APP_INSTANCE")
	if !ok {
		appInstance = DefaultAppInstance
	}

	appConfigName, ok := os.LookupEnv("UNIM_APP_CONFIG_PATH")
	if !ok {
		appConfigName = DefaultConfigPath
	}

	return &DeployInfo{
		DeployEnv:     deployEnv,
		AppName:       appName,
		AppInstance:   appInstance,
		AppConfigName: appConfigName,
	}, nil
}

package api

import (
	"encoding/json"
	"fmt"
	"net/http"

	apiHelpers "bitbucket.org/unimhq/core/pkg/helpers/api"
	"bitbucket.org/unimhq/core/pkg/services"
	apiTypes "bitbucket.org/unimhq/core/pkg/types/api"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

type HandlerWithReturn func(w http.ResponseWriter, req *http.Request) (*apiTypes.Response, error)
type FieldExtractor func(req *http.Request) (zap.Field, error)

func ResponseHandler(withReturn HandlerWithReturn, extractors ...FieldExtractor) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		resp, err := withReturn(w, req)

		translateErrorToHTTPStatus := false

		if err != nil {
			fields := make([]zapcore.Field, 0, 0)

			if len(extractors) > 0 {
				for _, extractor := range extractors {
					field, err := extractor(req)

					if err != nil {
						continue
					}

					fields = append(fields, field)
				}
			}

			message := "internal error was occurred"

			switch err.(type) {
			case *apiTypes.ApiInternalError:
				apiInternalError := err.(*apiTypes.ApiInternalError)
				message = fmt.Sprintf("%v, %v", apiInternalError.MethodName, apiInternalError.Message)

				if apiInternalError.TranslateToHttpStatus == true {
					translateErrorToHTTPStatus = true
				}

				if apiInternalError.MethodParams != nil {
					jsonMethodParams, err := json.Marshal(apiInternalError.MethodParams)

					if err != nil {
						fields = append(fields, zap.String("params", string(jsonMethodParams)))
					} else {
						fields = append(fields, zap.Any("params", apiInternalError.MethodParams))
					}
				}

				if apiInternalError.Context != nil {
					for key, val := range apiInternalError.Context {
						fields = append(fields, zap.Any(key, val))
					}
				}

				if apiInternalError.Err != nil {
					fields = append(fields, zap.Error(apiInternalError.Err))
				}

			default:
				fields = append(fields, zap.Error(err))
			}

			services.Application().Logger().Error(message, fields...)
			resp = apiHelpers.InternalServerError()

			if translateErrorToHTTPStatus == true {
				resp.TranslateErrorToHttpStatus = true
			}
		}

		if resp.ErrorCode != 0 && resp.TranslateErrorToHttpStatus == true {
			w.WriteHeader(apiTypes.ErrorCodeToHTTPStatusCode(resp.ErrorCode))
		}

		if resp != nil {
			w.Header().Add("Content-Type", "application/json")
			apiHelpers.ToJSON(w, *resp)
		}
	})
}

package middlewares

import (
	"errors"
	"net/http"

	apiHelper "bitbucket.org/unimhq/core/pkg/helpers/api"
	"bitbucket.org/unimhq/core/pkg/services"
	apiTypes "bitbucket.org/unimhq/core/pkg/types/api"
	"go.uber.org/zap"
)

//ErrorInterceptor recover panics in "next" and log error
type ErrorInterceptor struct {
	logger *zap.Logger
}

func (m *ErrorInterceptor) ServeHTTP(w http.ResponseWriter, req *http.Request, next http.HandlerFunc) {
	defer func() {
		if err := recover(); err != nil {
			switch err.(type) {
			case apiTypes.MalformedApiParams:
				services.Application().Logger().Error("Error interceptor was detect malformed params errors",
					zap.Error(err.(apiTypes.MalformedApiParams).Err),
					zap.Stack("stacktrace"))
				apiHelper.ToJSON(w, *apiHelper.MalformedParamsError())
			default:
				services.Application().Logger().Error("Error interceptor was detect an error",
					zap.Any("error", err),
					zap.Stack("stacktrace"))
				apiHelper.ToJSON(w, *apiHelper.InternalServerError())
			}

			return
		}
	}()
	next.ServeHTTP(w, req)
}

//NewErrorInterceptor constructor
func NewErrorInterceptor(logger *zap.Logger) (*ErrorInterceptor, error) {
	if logger == nil {
		return nil, errors.New("logger can't be empty")
	}

	return &ErrorInterceptor{
		logger: logger,
	}, nil
}

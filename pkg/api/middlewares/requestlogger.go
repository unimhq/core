package middlewares

import (
	apiHelper "bitbucket.org/unimhq/core/pkg/helpers/api"
	"errors"
	"go.uber.org/zap"
	"net/http"
)

//RequestLogger logs all requests
type RequestLogger struct {
	logger        *zap.Logger
	requestLogger *zap.Logger
	message       string
}

func (m *RequestLogger) ServeHTTP(w http.ResponseWriter, req *http.Request, next http.HandlerFunc) {
	apiHelper.Log(req, m.requestLogger, m.message)
	next.ServeHTTP(w, req)
}

//NewRequestLogger constructor
func NewRequestLogger(logger *zap.Logger, requestLogger *zap.Logger, message string) (*RequestLogger, error) {
	if logger == nil {
		return nil, errors.New("logger can't be empty")
	}

	if requestLogger == nil {
		return nil, errors.New("requestLogger can't be empty")
	}

	resultMessage := "Request received"

	if len(message) > 0 {
		resultMessage = message
	}

	return &RequestLogger{
		logger:        logger,
		requestLogger: requestLogger,
		message:       resultMessage,
	}, nil
}

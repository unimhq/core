package middlewares

import (
	apiHelpers "bitbucket.org/unimhq/core/pkg/helpers/api"
	jwtHelpers "bitbucket.org/unimhq/core/pkg/helpers/jwt"
	jwtTypes "bitbucket.org/unimhq/core/pkg/types/jwt"
	"context"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/pkg/errors"
	"go.uber.org/zap"
	"net/http"
	"strings"
)

// A function called whenever an error is encountered
type jwtCheckerErrorHandler func(w http.ResponseWriter, r *http.Request, err error)

// TokenExtractor is a function that takes a request as input and returns
// either a token or an error.  An error should only be returned if an attempt
// to specify a token was found, but the information was somehow incorrectly
// formed.  In the case where a token is simply not present, this should not
// be treated as an error.  An empty string should be returned in that case.
type TokenExtractor func(r *http.Request) (string, error)

//DefaultJwtCheckerErrorHandler ...
type DefaultJwtCheckerErrorHandler struct {
	logger *zap.Logger
}

func (h *DefaultJwtCheckerErrorHandler) HandleError(w http.ResponseWriter, r *http.Request, err error) {
	response := jwtHelpers.TokenErrorResponse(err)

	if response == nil {
		return
	}

	apiHelpers.ToJSON(w, *response)
}

func NewDefaultJwtCheckerErrorHandler(logger *zap.Logger) (*DefaultJwtCheckerErrorHandler, error) {
	if logger == nil {
		return nil, errors.New("NewDefaultJwtCheckerErrorHandler, logger must be not empty")
	}

	return &DefaultJwtCheckerErrorHandler{logger: logger}, nil
}

type JwtChecker struct {
	logger  *zap.Logger
	options JwtCheckerOptions
}

type JwtCheckerOptions struct {
	// The function that will return the Key to validate the JWT.
	// It can be either a shared secret or a public key.
	// Default value: nil
	ValidationKeyGetter jwt.Keyfunc
	// The name of the property in the request where the token data contains
	// from the JWT will be stored.
	// Default value: "jwt"
	TokenProperty string
	// The function that will be called when there's an error validating the token
	// Default value:
	ErrorHandler jwtCheckerErrorHandler
	// A function that extracts the token from the request
	// Default: FromAuthHeader (i.e., from Authorization header as bearer token)
	Extractor TokenExtractor
	// When set, the middelware verifies that tokens are signed with the specific signing algorithm
	// If the signing method is not constant the ValidationKeyGetter callback can be used to implement additional checks
	// Important to avoid security issues described here: https://auth0.com/blog/2015/03/31/critical-vulnerabilities-in-json-web-token-libraries/
	// Default: nil
	SigningMethod jwt.SigningMethod

	Claims jwt.Claims

	NeedClaimsValidation bool
}

// FromAuthHeader is a "TokenExtractor" that takes a give request and extracts
// the JWT token from the Authorization header.
func FromAuthHeader(r *http.Request) (string, error) {
	authHeader := r.Header.Get("Authorization")
	if authHeader == "" {
		return "", nil
	}

	// TODO: Make this a bit more robust, parsing-wise
	authHeaderParts := strings.Split(authHeader, " ")
	if len(authHeaderParts) != 2 || strings.ToLower(authHeaderParts[0]) != "bearer" {
		err := jwtTypes.NewAuthHeaderFormatError(errors.New("Authorization header format must be Bearer {token}"))
		return "", &err
	}

	return authHeaderParts[1], nil
}

func (m *JwtChecker) ServeHTTP(w http.ResponseWriter, req *http.Request, next http.HandlerFunc) {
	err := m.check(w, req)
	if err != nil {
		m.logger.Info("JwtChecker.ServeHTTP, jwt token check failed", zap.Error(err), zap.Any("url", req.URL))
		return
	}

	next(w, req)
}

func (m *JwtChecker) check(w http.ResponseWriter, req *http.Request) error {
	token, err := m.options.Extractor(req)

	if err != nil {
		m.options.ErrorHandler(w, req, err)
		return errors.Wrap(err, "JwtChecker.check, unable to extract token")
	}

	if len(token) == 0 {
		err := jwtTypes.NewTokenNotFoundError(errors.New("Authorization token not found"))
		m.options.ErrorHandler(w, req, &err)
		return errors.Wrap(err.Err, "JwtChecker.check, token was not found")
	}

	parser := jwt.Parser{SkipClaimsValidation: !m.options.NeedClaimsValidation}
	parsedToken, err := parser.ParseWithClaims(token, m.options.Claims, m.options.ValidationKeyGetter)

	if err != nil {
		m.options.ErrorHandler(w, req, err)
		return errors.Wrap(err, fmt.Sprintf("JwtChecker.check, unable to parse token, token: %v", token))
	}

	if m.options.SigningMethod != nil && m.options.SigningMethod.Alg() != parsedToken.Header["alg"] {
		err := jwtTypes.NewTokenAlgValidationError(errors.New(fmt.Sprintf("Expected %s signing method but token specified %s",
			m.options.SigningMethod.Alg(),
			parsedToken.Header["alg"])))
		m.options.ErrorHandler(w, req, &err)
		return errors.Wrap(err.Err, "JwtChecker.check, token validating algorithms mismatched")
	}

	if !parsedToken.Valid {
		err := jwtTypes.NewTokenValidationError(errors.New("Token is invalid"))
		m.options.ErrorHandler(w, req, &err)
		return errors.New("Token is invalid")
	}

	*req = *req.WithContext(context.WithValue(req.Context(), m.options.TokenProperty, parsedToken))

	return nil
}

func NewJwtChecker(logger *zap.Logger, customOptions *JwtCheckerOptions) (*JwtChecker, error) {
	if logger == nil {
		return nil, errors.New("NewJwtChecker, logger must be not empty")
	}

	defaultErrorHandler, err := NewDefaultJwtCheckerErrorHandler(logger)

	if err != nil {
		return nil, errors.Wrap(err, "NewJwtChecker, unable to create default error handler")
	}

	options := JwtCheckerOptions{
		TokenProperty:        "jwt",
		ErrorHandler:         defaultErrorHandler.HandleError,
		Extractor:            FromAuthHeader,
		Claims:               new(jwt.MapClaims),
		NeedClaimsValidation: customOptions.NeedClaimsValidation,
	}

	if customOptions != nil {
		if len(customOptions.TokenProperty) > 0 {
			options.TokenProperty = customOptions.TokenProperty
		}

		if customOptions.ErrorHandler != nil {
			options.ErrorHandler = customOptions.ErrorHandler
		}

		if customOptions.Extractor != nil {
			options.Extractor = customOptions.Extractor
		}

		if customOptions.ValidationKeyGetter != nil {
			options.ValidationKeyGetter = customOptions.ValidationKeyGetter
		}

		if customOptions.SigningMethod != nil {
			options.SigningMethod = customOptions.SigningMethod
		}

		if customOptions.Claims != nil {
			options.Claims = customOptions.Claims
		}
	}

	return &JwtChecker{logger: logger, options: options}, nil
}

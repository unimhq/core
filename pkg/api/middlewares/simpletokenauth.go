package middlewares

import (
	"net/http"

	"github.com/pkg/errors"
	"go.uber.org/zap"
)

const (
	SimpleTokenAuthAppNameHeader = "X-Auth-App"
	SimpleTokenAuthSecretHeader  = "X-Auth-Secret"
)

//SimpleTokenAuth provide authentication by tokens in HTTP-headers
type SimpleTokenAuth struct {
	appName string
	secret  string
	logger  *zap.Logger
}

func (m *SimpleTokenAuth) ServeHTTP(w http.ResponseWriter, req *http.Request, next http.HandlerFunc) {
	appName := req.Header.Get(SimpleTokenAuthAppNameHeader)
	secret := req.Header.Get(SimpleTokenAuthSecretHeader)

	if appName == m.appName && secret == m.secret {
		next.ServeHTTP(w, req)
		return
	}

	m.logger.Warn("Simple token auth failure",
		zap.String("appName", m.appName),
		zap.String("sentAppName", appName))

	w.WriteHeader(http.StatusForbidden)
}

//NewSimpleTokenAuth constructor
func NewSimpleTokenAuth(logger *zap.Logger, appName string, secret string) (*SimpleTokenAuth, error) {
	if logger == nil {
		return nil, errors.New("middlewares.NewSimpleTokenAuth, logger must be not empty")
	}

	if len(appName) == 0 {
		return nil, errors.New("middlewares.NewSimpleTokenAuth, appName must be not empty")
	}

	if len(secret) == 0 {
		return nil, errors.New("middlewares.NewSimpleTokenAuth, secret must be not empty")
	}

	return &SimpleTokenAuth{
		appName: appName,
		secret:  secret,
		logger:  logger,
	}, nil
}

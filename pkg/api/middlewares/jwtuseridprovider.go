package middlewares

import (
	coreApiHelper "bitbucket.org/unimhq/core/pkg/helpers/api"
	coreJwt "bitbucket.org/unimhq/core/pkg/types/jwt"
	"context"
	"github.com/dgrijalva/jwt-go"
	"github.com/pkg/errors"
	"go.uber.org/zap"
	"net/http"
	"reflect"
)

//JwtUserIdProvider checks token expiration
type JwtUserIdProvider struct {
	logger *zap.Logger
}

func (m *JwtUserIdProvider) ServeHTTP(w http.ResponseWriter, req *http.Request, next http.HandlerFunc) {
	jwtData := req.Context().Value("jwt")
	token, ok := jwtData.(*jwt.Token)

	if ok != true {
		m.logger.Error("JwtUserIdProvider.ServeHTTP, unable to get jwt from context")
		return
	}

	claims, ok := token.Claims.(*coreJwt.UnimClaims)

	if ok != true {
		m.logger.Error("JwtUserIdProvider.ServeHTTP, wrong token claims type (expected bitbucket.org/unimhq/core/pkg/types/jwt#UnimJwt)",
			zap.Any("claims", token.Claims),
			zap.Any("claimsType", reflect.TypeOf(token.Claims)))

		coreApiHelper.ToJSON(w, *coreApiHelper.InternalServerError())
		return
	}

	if err := claims.Valid(); err != nil {
		m.logger.Error("JwtUserIdProvider.ServeHTTP, token claims is not valid",
			zap.Any("claims", token.Claims),
			zap.Error(err))

		coreApiHelper.ToJSON(w, *coreApiHelper.InternalServerError())
		return
	}

	if len(claims.UserID) == 0 {
		m.logger.Error("JwtUserIdProvider.ServeHTTP, unable to find user id in token claims userId",
			zap.Any("claims", token.Claims))
		coreApiHelper.ToJSON(w, *coreApiHelper.InternalServerError())
		return
	}

	req = req.WithContext(context.WithValue(req.Context(), "userId", claims.UserID))
	next.ServeHTTP(w, req)
}

func NewJwtUserIdProvider(logger *zap.Logger) (*JwtUserIdProvider, error) {
	if logger == nil {
		return nil, errors.New("NewJwtUserIdProvider, logger must be not empty")
	}

	return &JwtUserIdProvider{logger: logger}, nil
}

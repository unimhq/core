package machinerylogs

import (
	"fmt"
	"github.com/RichardKnop/logging"
	"go.uber.org/zap"
	"os"
)

//Данные адаптеры нужны для подключения к machinery логгера ZAP

//Info logger
type infoLogger struct {
	logger *zap.Logger
}

func (l *infoLogger) Print(params ...interface{}) {
	l.logger.Info(fmt.Sprint(params...))
}

func (l *infoLogger) Printf(str string, params ...interface{}) {
	if len(params) == 0 {
		l.logger.Info(str)
		return
	}

	l.logger.Info(fmt.Sprintf(str, params...))
}

func (l *infoLogger) Println(params ...interface{}) {
	for _, param := range params {
		l.logger.Info(fmt.Sprint(param))
	}
}

func (l *infoLogger) Fatal(params ...interface{}) {
	l.Print(params...)
	os.Exit(1)
}

func (l *infoLogger) Fatalf(str string, params ...interface{}) {
	l.Printf(str, params...)
	os.Exit(1)
}

func (l *infoLogger) Fatalln(params ...interface{}) {
	l.Println(params)
	os.Exit(1)
}

func (l *infoLogger) Panic(params ...interface{}) {
	message := fmt.Sprint(params...)
	l.logger.Info(message)
	panic(message)
}

func (l *infoLogger) Panicf(str string, params ...interface{}) {
	if len(params) == 0 {
		l.logger.Info(str)
		panic(str)
	}

	message := fmt.Sprintf(str, params...)
	l.logger.Info(message)
	panic(message)
}

func (l *infoLogger) Panicln(params ...interface{}) {
	result := ""
	for _, param := range params {
		result += fmt.Sprintln(param)
	}

	l.logger.Info(result)
	panic(result)
}

//NewInfoLogger constructor for log machinery info level messages
func NewInfoLogger(logger *zap.Logger) logging.LoggerInterface {
	return &infoLogger{
		logger: logger,
	}
}

//Warn logger

type warnLogger struct {
	logger *zap.Logger
}

func (l *warnLogger) Print(params ...interface{}) {
	l.logger.Warn(fmt.Sprint(params...))
}

func (l *warnLogger) Printf(str string, params ...interface{}) {
	if len(params) == 0 {
		l.logger.Warn(str)
		return
	}

	l.logger.Warn(fmt.Sprintf(str, params...))
}

func (l *warnLogger) Println(params ...interface{}) {
	for _, param := range params {
		l.logger.Warn(fmt.Sprint(param))
	}
}

func (l *warnLogger) Fatal(params ...interface{}) {
	l.Print(params...)
	os.Exit(1)
}

func (l *warnLogger) Fatalf(str string, params ...interface{}) {
	l.Printf(str, params...)
	os.Exit(1)
}

func (l *warnLogger) Fatalln(params ...interface{}) {
	l.Println(params)
	os.Exit(1)
}

func (l *warnLogger) Panic(params ...interface{}) {
	message := fmt.Sprint(params...)
	l.logger.Warn(message)
	panic(message)
}

func (l *warnLogger) Panicf(str string, params ...interface{}) {
	if len(params) == 0 {
		l.logger.Warn(str)
		panic(str)
	}

	message := fmt.Sprintf(str, params...)
	l.logger.Warn(message)
	panic(message)
}

func (l *warnLogger) Panicln(params ...interface{}) {
	result := ""
	for _, param := range params {
		result += fmt.Sprintln(param)
	}

	l.logger.Warn(result)
	panic(result)
}

//NewWarnLogger constructor for log machinery warn level messages
func NewWarnLogger(logger *zap.Logger) logging.LoggerInterface {
	return &warnLogger{
		logger: logger,
	}
}

//Error logger

type errorLogger struct {
	logger *zap.Logger
}

func (l *errorLogger) Print(params ...interface{}) {
	l.logger.Error(fmt.Sprint(params...))
}

func (l *errorLogger) Printf(str string, params ...interface{}) {
	if len(params) == 0 {
		l.logger.Error(str)
		return
	}

	l.logger.Error(fmt.Sprintf(str, params...))
}

func (l *errorLogger) Println(params ...interface{}) {
	for _, param := range params {
		l.logger.Error(fmt.Sprint(param))
	}
}

func (l *errorLogger) Fatal(params ...interface{}) {
	l.Print(params...)
	os.Exit(1)
}

func (l *errorLogger) Fatalf(str string, params ...interface{}) {
	l.Printf(str, params...)
	os.Exit(1)
}

func (l *errorLogger) Fatalln(params ...interface{}) {
	l.Println(params)
	os.Exit(1)
}

func (l *errorLogger) Panic(params ...interface{}) {
	message := fmt.Sprint(params...)
	l.logger.Error(message)
	panic(message)
}

func (l *errorLogger) Panicf(str string, params ...interface{}) {
	if len(params) == 0 {
		l.logger.Error(str)
		panic(str)
	}

	message := fmt.Sprintf(str, params...)
	l.logger.Error(message)
	panic(message)
}

func (l *errorLogger) Panicln(params ...interface{}) {
	result := ""
	for _, param := range params {
		result += fmt.Sprintln(param)
	}

	l.logger.Error(result)
	panic(result)
}

//NewErrorLogger constructor for log machinery error level messages
func NewErrorLogger(logger *zap.Logger) logging.LoggerInterface {
	return &errorLogger{
		logger: logger,
	}
}

//Fatal logger

type fatalLogger struct {
	logger *zap.Logger
}

func (l *fatalLogger) Print(params ...interface{}) {
	l.logger.Fatal(fmt.Sprint(params...))
}

func (l *fatalLogger) Printf(str string, params ...interface{}) {
	if len(params) == 0 {
		l.logger.Fatal(str)
		return
	}

	l.logger.Fatal(fmt.Sprintf(str, params...))
}

func (l *fatalLogger) Println(params ...interface{}) {
	for _, param := range params {
		l.logger.Fatal(fmt.Sprint(param))
	}
}

func (l *fatalLogger) Fatal(params ...interface{}) {
	l.Print(params...)
	os.Exit(1)
}

func (l *fatalLogger) Fatalf(str string, params ...interface{}) {
	l.Printf(str, params...)
	os.Exit(1)
}

func (l *fatalLogger) Fatalln(params ...interface{}) {
	l.Println(params)
	os.Exit(1)
}

func (l *fatalLogger) Panic(params ...interface{}) {
	message := fmt.Sprint(params...)
	l.logger.Fatal(message)
	panic(message)
}

func (l *fatalLogger) Panicf(str string, params ...interface{}) {
	if len(params) == 0 {
		l.logger.Fatal(str)
		panic(str)
	}

	message := fmt.Sprintf(str, params...)
	l.logger.Fatal(message)
	panic(message)
}

func (l *fatalLogger) Panicln(params ...interface{}) {
	result := ""
	for _, param := range params {
		result += fmt.Sprintln(param)
	}

	l.logger.Fatal(result)
	panic(result)
}

//NewFatalLogger constructor for log machinery fatal level messages
func NewFatalLogger(logger *zap.Logger) logging.LoggerInterface {
	return &fatalLogger{
		logger: logger,
	}
}

package validation

import "github.com/pkg/errors"

func ExtractString(value interface{}) (*string, error) {
	var result string
	switch value.(type) {
	case *string:
		val := value.(*string)

		if val == nil {
			return nil, nil
		}

		result = *val
	case string:
		result = value.(string)
	default:
		return nil, errors.New("must be a string")
	}

	return &result, nil
}

func ExtractFloat64(value interface{}) (*float64, error) {
	var result float64
	switch value.(type) {
	case *float64:
		val := value.(*float64)

		if val == nil {
			return nil, nil
		}

		result = *val
	case float64:
		result = value.(float64)
	default:
		return nil, errors.New("must be a float64")
	}

	return &result, nil
}

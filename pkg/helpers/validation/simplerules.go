package validation

import (
	"github.com/pkg/errors"
	"golang.org/x/text/currency"
	"regexp"
)

//E.164 spec
var phoneRegexp = regexp.MustCompile(`^\+?[1-9]\d{1,14}$`)
var boolRegexp = regexp.MustCompile(`^(true|false)$`)

//IsPhoneNumber validation rule for phone number validation
func IsPhoneNumber(value interface{}) error {
	var phone *string

	phone, err := ExtractString(value)

	if err != nil {
		return err
	}

	if phone == nil {
		return nil
	}

	result := phoneRegexp.Match([]byte(*phone))

	if result != true {
		return errors.New("must be right phone number")
	}

	return nil
}

func IsBool(value interface{}) error {
	var strValue string

	switch value.(type) {
	case *string:
		val := value.(*string)

		if val == nil {
			return nil
		}

		strValue = *val
	case string:
		strValue = value.(string)
	case bool:
		return nil
	default:
		return errors.New("must be a string")
	}

	result := boolRegexp.Match([]byte(strValue))

	if result != true {
		return errors.New("only true or false available")
	}

	return nil
}

func IsCurrencyValue(value interface{}) error {
	var strValue *string

	strValue, err := ExtractString(value)

	if err != nil {
		return err
	}

	if strValue == nil {
		return nil
	}

	_, err = currency.ParseISO(*strValue)

	if err != nil {
		return errors.New("wrong currency format")
	}

	return nil
}

func AlwaysWrong(value interface{}) error {
	return errors.New("wrong value")
}

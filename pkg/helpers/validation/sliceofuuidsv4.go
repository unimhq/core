package validation

import (
	"github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/pkg/errors"
	"reflect"
)

func SliceOfUUIDsV4() validation.Rule {
	return &sliceOfUUIDsV4{}
}

type sliceOfUUIDsV4 struct {
}

func (r *sliceOfUUIDsV4) Validate(value interface{}) error {
	reflectValue := reflect.ValueOf(value)

	if reflectValue.Kind() != reflect.Slice {
		return errors.New("wrong value")
	}

	elementsCount := reflectValue.Len()
	for i := 0; i < elementsCount; i++ {
		err := validation.Validate(reflectValue.Index(i).Interface(), is.UUIDv4)

		if err != nil {
			return errors.New("Elements in array must be UUIDs V4")

		}
	}

	return nil
}

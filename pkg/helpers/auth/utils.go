package auth

import (
	"github.com/pkg/errors"
	"golang.org/x/crypto/bcrypt"
)

//CheckPassword compare password with hashed value
func CheckPassword(hashedPassword string, password string) error {
	return bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))
}

//HashPassword hash password and return it
func HashPassword(password string) (string, error) {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)

	if err != nil {
		return "", errors.Wrap(err, "auth.HashPassword, unable to hash password")
	}

	return string(hashedPassword), nil
}

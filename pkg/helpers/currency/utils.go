package currency

import "github.com/shopspring/decimal"

const (
	fiatDecimals = 100
)

func NormalizeFiat(amount float64) decimal.Decimal {
	return normalize(amount, fiatDecimals)
}

func DenormalizeFiat(amount decimal.Decimal) decimal.Decimal {
	return denormalize(amount, fiatDecimals)
}

func normalize(amount float64, decimals int64) decimal.Decimal {
	return decimal.NewFromFloat(amount).Mul(decimal.New(decimals, 0))
}

func denormalize(amount decimal.Decimal, decimals int64) decimal.Decimal {
	return amount.DivRound(decimal.New(decimals, 0), 100)
}

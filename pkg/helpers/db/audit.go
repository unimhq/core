package db

import (
	sq "github.com/Masterminds/squirrel"
	"time"
)

// conventional column name for soft delete
var CreatedAtColumnName = "created_at"
var UpdatedAtColumnName = "updated_at"
var DeletedColumnName = "deleted_at"

var NotDeletedPredicate = sq.Eq{DeletedColumnName: nil}

type Auditable interface { // todo(egor): add helpers
	getCreatedAt() *time.Time
	getUpdatedAt() *time.Time
}

type SoftDeletable interface {
	getDeletedAt() *time.Time
	IsDeleted() bool
}

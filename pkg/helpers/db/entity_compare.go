package db

import (
	"encoding/json"
	"fmt"
	"github.com/fatih/structs"
	"github.com/pkg/errors"
	"github.com/yudai/gojsondiff"
)

// check only public fields
// skip audit fields
func IsEntityEdited(oldEntity interface{}, newEntity interface{}) bool {
	oldAsMap := structs.Map(oldEntity)
	newAsMap := structs.Map(newEntity)

	// skip some fields
	skipFields := []string{"CreatedAt", "UpdatedAt", "DeletedAt"}
	skipMapFields(&oldAsMap, skipFields)
	skipMapFields(&newAsMap, skipFields)

	oldAsJson, err := json.Marshal(oldAsMap)
	if err != nil {
		panic(errors.Wrap(err, fmt.Sprintf("Can't serialize to json %+v", oldEntity)))
	}

	newAsJson, err := json.Marshal(newAsMap)
	if err != nil {
		panic(errors.Wrap(err, fmt.Sprintf("Can't serialize to json %+v", newEntity)))
	}

	differ := gojsondiff.New()

	diff, err := differ.Compare(oldAsJson, newAsJson)

	if err != nil {
		panic(errors.Wrap(err, fmt.Sprintf("Can't diff %+v / %+v", newEntity, oldEntity)))
	}

	return diff.Modified()
}

func skipMapFields(m *map[string]interface{}, fields []string) {
	for _, field := range fields {
		delete(*m, field)
	}
	println("", m)
}

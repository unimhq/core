package db

import (
	"bitbucket.org/unimhq/core/pkg/helpers/timeformat"
	"bitbucket.org/unimhq/core/pkg/types"
	"context"
	"database/sql"
	"fmt"
	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"github.com/satori/go.uuid"
	"reflect"
	"time"
)

const dbTxContextKey = "dbTx"

func MustCommit(tx *sqlx.Tx) {
	err := tx.Commit()

	if err != nil {
		panic(errors.Wrap(err, "db.MustCommit, unable to commit transaction"))
	}
}

func MustRollback(tx *sqlx.Tx) {
	err := tx.Rollback()

	if err != nil {
		panic(errors.Wrap(err, "db.MustRollback, unable to rollback transaction"))
	}
}

func MustRollbackIfNeeded(externalTransaction *sqlx.Tx, transaction *sqlx.Tx) {
	if externalTransaction == nil {
		MustRollback(transaction)
	}
}

func MustCommitIfNeeded(externalTransaction *sqlx.Tx, transaction *sqlx.Tx) {
	if externalTransaction == nil {
		MustCommit(transaction)
	}
}

func EmptyOrError(err error, errorMessage string) error {
	if err == sql.ErrNoRows {
		return nil
	}
	return errors.Wrap(err, errorMessage)
}

func RollbackOnError(e error, t *sqlx.Tx) {
	if recoverError := recover(); recoverError != nil {
		MustRollback(t)
		panic(recoverError)
	}
	if e != nil {
		MustRollback(t)
	}
}

func PutTransactionToContext(ctx context.Context, tx sqlx.Tx) context.Context {
	ctx = context.WithValue(ctx, dbTxContextKey, tx)

	return ctx
}

func GetTransactionFromContext(ctx context.Context) *sqlx.Tx {
	dbTxInterface := ctx.Value(dbTxContextKey)

	if dbTxInterface == nil {
		return nil
	}

	dbTx, ok := dbTxInterface.(sqlx.Tx)

	if ok != true {
		return nil
	}

	return &dbTx
}

func GetFromContextOrUseExisting(ctx context.Context, db sqlx.Ext) sqlx.Ext {
	tx := GetTransactionFromContext(ctx)

	if tx != nil {
		return tx
	}

	return db
}

func FetchByChunks(query string, queryArgs []interface{}, chunkSize int, db sqlx.DB, resultType reflect.Type, retChan chan types.AsyncResult) {
	tx := db.MustBegin()
	defer func() {
		tx.Commit()

		if recoverError := recover(); recoverError != nil {
			retChan <- types.AsyncResult{Error: errors.New(fmt.Sprintf("dbUtils.FetchByChunks, unexpected error occurred, details: %v", recoverError))}
		}
		close(retChan)
	}()

	cursorName := fmt.Sprintf("chunk-select-%v", uuid.NewV4())
	q := fmt.Sprintf("DECLARE \"%v\" CURSOR FOR %v", cursorName, query)
	_, err := tx.Exec(q, queryArgs...)

	if err != nil {
		retChan <- types.AsyncResult{Error: errors.Wrap(err, "dbUtils.FetchByChunks, unable to execute declare cursor SQL query")}
		return
	}

	for {
		rows, err := tx.Queryx(fmt.Sprintf("FETCH %d FROM \"%v\"", chunkSize, cursorName))
		if err != nil {
			retChan <- types.AsyncResult{Error: errors.Wrap(err, "dbUtils.FetchByChunks, unable to execute fetch data SQL query")}
			return
		}

		resultSlice := reflect.MakeSlice(reflect.SliceOf(resultType), 0, 0)
		result := reflect.New(resultType)

		for rows.Next() {
			err = rows.StructScan(result.Interface())

			if err != nil {
				retChan <- types.AsyncResult{Error: errors.Wrap(err, fmt.Sprintf("dbUtils.FetchByChunks, unable to scan query result to struct: `%v`", resultType.String()))}
				return
			}

			resultSlice = reflect.Append(resultSlice, result.Elem())
		}

		retChan <- types.AsyncResult{Value: resultSlice.Interface(), Error: nil}

		if resultSlice.Len() < chunkSize {
			break
		}
	}
}

func BuildPGTimezone(dt time.Time) string {
	name, utcOffset := dt.Zone()

	if len(name) > 0 {
		return name
	}

	utcOffset *= -1

	d, err := time.ParseDuration(fmt.Sprintf("%vs", utcOffset))

	if err != nil {
		panic("ExtractPGTimezone, unable to parse utc offset")
	}

	return timeformat.DurationToHMS(d)
}

type RunWithTransaction func(db sqlx.Ext) error

// If transaction already started caller must handle errors and rollback if required
// If no external transaction exists - start new transaction and commit on success, rollback - on error after block execution
func RunInTransaction(ext sqlx.Ext, block RunWithTransaction) error {
	createdTransaction, extractTxError := extractTx(ext)

	if extractTxError != nil {
		return errors.Wrap(extractTxError, "Can't extract transaction")
	}

	blockError := block(createdTransaction.tx)

	if blockError != nil {
		if createdTransaction.isNew { // rollback only new transaction, for old - delegate tx management to caller
			rollbackError := createdTransaction.tx.Rollback()
			if rollbackError != nil {
				return errors.Wrap(rollbackError, fmt.Sprintf("Can't rollback transactiona after error %s", blockError))
			}
		}

		return errors.Wrap(blockError, "Block run failed")
	}

	if createdTransaction.isNew {
		commitError := createdTransaction.tx.Commit()
		if commitError != nil {
			return errors.Wrap(commitError, "Can't commit transaction after block run")
		}
	}

	return nil
}

type createdTransaction struct {
	isNew bool
	tx    *sqlx.Tx
}

func extractTx(ext sqlx.Ext) (*createdTransaction, error) {
	currentTx, ok := ext.(*sqlx.Tx)
	if ok {
		return &createdTransaction{false, currentTx}, nil
	}

	db, ok := ext.(*sqlx.DB)

	if !ok {
		return nil, errors.New("Unsupported type of sqx.Ext")
	}

	newTx := db.MustBegin()

	return &createdTransaction{true, newTx}, nil
}

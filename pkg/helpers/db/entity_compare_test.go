package db

import (
	"gotest.tools/assert"
	is "gotest.tools/assert/cmp"
	"testing"
	"time"
)

type someStruct struct {
	Field1       string
	Field2       int64
	CreatedAt    time.Time
	UpdatedAt    time.Time
	DeletedAt    time.Time
	privateField int64
}

func TestIsEntityEditedForDifferentEntities(t *testing.T) {
	// arrange
	now := time.Now()
	tomorrow := now.Add(time.Hour * 24)
	oldItem := someStruct{"x", 42, now, now, now, 13}
	newItem := someStruct{"x", 43, tomorrow, tomorrow, tomorrow, 12}

	// act
	result := IsEntityEdited(oldItem, newItem)

	// assert
	assert.Assert(t, is.Equal(result, true))
}

func TestIsEntityEditedForSameEntities(t *testing.T) {
	// arrange
	now := time.Now()
	tomorrow := now.Add(time.Hour * 24)
	oldItem := someStruct{"x", 42, now, now, now, 13}
	newItem := someStruct{"x", 42, tomorrow, tomorrow, tomorrow, 12}

	// act
	result := IsEntityEdited(oldItem, newItem)

	// assert
	assert.Assert(t, is.Equal(result, false))
}

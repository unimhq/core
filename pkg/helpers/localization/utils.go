package localization

import (
	"bitbucket.org/unimhq/core/pkg/types"
	coreErrors "bitbucket.org/unimhq/core/pkg/types/errors"
	"github.com/nicksnyder/go-i18n/v2/i18n"
)

func Localize(localizer *i18n.Localizer, key string, data interface{}, fallbackValue *string) string {
	if localizer == nil {
		panic("localizer must be not empty")
	}

	localized, err := localizer.Localize(&i18n.LocalizeConfig{MessageID: key, TemplateData: data})
	if err != nil {
		if fallbackValue != nil {
			return *fallbackValue
		}
		return key
	}

	return localized
}

func TryToLocalize(localizer *i18n.Localizer, obj interface{}, fallbackValue *string) string {
	if localizer == nil {
		panic("localizer must be not empty")
	}

	loc, ok := obj.(types.Localizable)

	if ok != true {
		if fallbackValue != nil {
			return *fallbackValue
		}

		return ""
	}

	return Localize(localizer, loc.GetLocalizationKey(), nil, fallbackValue)
}

func TryToLocalizeError(localizer *i18n.Localizer, err error) string {
	if localizer == nil {
		panic("localizer must be not empty")
	}

	if err == nil {
		return ""
	}

	switch err.(type) {
	case *coreErrors.Public:
		return Localize(localizer, err.(*coreErrors.Public).MessageID, nil, nil)
	default:
		return err.Error()
	}
}

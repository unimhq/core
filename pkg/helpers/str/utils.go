package str

import (
	"fmt"
	"github.com/microcosm-cc/bluemonday"
	"github.com/pkg/errors"
	"github.com/satori/go.uuid"
	"html"
	"strings"
	"unicode"
)

var stripTagPolicy *bluemonday.Policy

func FirstToLower(str string) string {
	if len(str) == 0 {
		return ""
	}
	result := []rune(str)
	result[0] = unicode.ToLower(result[0])
	return string(result)
}

func GetValue(value *string, defaultValue string) string {
	if value == nil {
		return defaultValue
	}

	return *value
}

func StripHTMLTags(str string) string {
	return html.UnescapeString(stripTagPolicy.Sanitize(str))
}

func RemoveLineBreaks(str string) string {
	tmp := strings.Replace(str, "\n", "", -1)
	tmp = strings.Replace(tmp, "\r", "", -1)

	return tmp
}

func MustConvertToUUIDv4(str string) uuid.UUID {
	result, err := uuid.FromString(str)

	if err != nil {
		panic(errors.New(fmt.Sprintf("Unable to to convert string to UUIDv4: `%s`", str)))
	}

	return result
}

func init() {
	stripTagPolicy = bluemonday.StrictPolicy()
}

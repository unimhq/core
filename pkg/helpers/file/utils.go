package file

import (
	"archive/zip"
	"fmt"
	"github.com/pkg/errors"
	"github.com/rakyll/magicmime"
	"io"
	"io/ioutil"
	"mime"
	"os"
	"path/filepath"
)

const (
	headerSizeInBytes = 512
)

func IsPathExists(path string) bool {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		return false
	}

	return true
}

func IsFileExists(path string) bool {
	info, err := os.Stat(path)
	if os.IsNotExist(err) {
		return false
	}

	return !info.IsDir()
}

func IsDirExists(path string) bool {
	info, err := os.Stat(path)
	if os.IsNotExist(err) {
		return false
	}
	return info.IsDir()
}

func MustSeekToStart(seeker io.Seeker) {
	_, err := seeker.Seek(0, 0)

	if err != nil {
		panic(errors.Wrap(err, "MustSeekToStart, unable to seek to start"))
	}
}

//DetectMimeType detect mime type of ReadSeeker
func DetectMimeType(readSeeker io.ReadSeeker) (string, error) {
	_, err := readSeeker.Seek(0, 0)

	if err != nil {
		return "", errors.Wrap(err, "DetectMimeType, unable to seek to start of reader")
	}

	buffer := make([]byte, headerSizeInBytes)
	_, err = readSeeker.Read(buffer)

	if err != nil {
		return "", errors.Wrap(err, "DetectMimeType, unable to read from reader")
	}

	dec, err := magicmime.NewDecoder(magicmime.MAGIC_MIME_TYPE | magicmime.MAGIC_NO_CHECK_COMPRESS | magicmime.MAGIC_ERROR)

	if err != nil {
		return "", errors.Wrap(err, "DetectMimeType, unable to create mime decoder")
	}

	defer dec.Close()

	mimeType, err := dec.TypeByBuffer(buffer)

	if err != nil {
		return "", errors.Wrap(err, "DetectMimeType, unable to get type by buffer")
	}

	if mimeType == "application/zip" || mimeType == "application/CDFV2" || mimeType == "application/octet-stream" {
		_, err = readSeeker.Seek(0, 0)
		if err != nil {
			return "", errors.Wrap(err, "DetectMimeType, unable to seek to start of reader")
		}

		deepSniffedType, err := mimeDeepSniff(readSeeker)

		if err != nil {
			return "", errors.Wrap(err, "DetectMimeType, unable to deep mime scan")
		}
		mimeType = deepSniffedType
	}

	_, err = readSeeker.Seek(0, 0)

	if err != nil {
		return "", errors.Wrap(err, "DetectMimeType, unable to seek to start of reader")
	}

	return mimeType, nil
}

//DetectExtensionByMimeType returns extension in format (.ext) for given mimeType
func DetectExtensionByMimeType(mimeType string) (string, error) {
	extensions, err := mime.ExtensionsByType(mimeType)

	if err != nil {
		return "", errors.Wrap(err, fmt.Sprintf("DetectExtensionByMimeType %v, unable to detect extension", mimeType))
	}

	if len(extensions) == 0 {
		return "", errors.Wrap(err, fmt.Sprintf("DetectExtensionByMimeType %v, unable to find extension for mime type", mimeType))
	}

	return extensions[0], nil
}

func IsImage(mimeType string) bool {
	return mimeType == "image/jpeg" || mimeType == "image/png" || mimeType == "image/gif"
}

func IsDocument(mimeType string) bool {
	return mimeType == "application/pdf" ||
		mimeType == "application/vnd.ms-excel" ||
		mimeType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" ||
		mimeType == "application/msword" ||
		mimeType == "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
}

// Unzip will decompress a zip archive, moving all files and folders
// within the zip file (parameter 1) to an output directory (parameter 2).
func Unzip(src string, dest string) ([]string, error) {

	var filenames []string

	r, err := zip.OpenReader(src)
	if err != nil {
		return filenames, err
	}
	defer r.Close()

	for _, f := range r.File {

		rc, err := f.Open()
		if err != nil {
			return filenames, err
		}
		defer rc.Close()

		// Store filename/path for returning and using later on
		fpath := filepath.Join(dest, f.Name)
		filenames = append(filenames, fpath)

		if f.FileInfo().IsDir() {

			// Make Folder
			os.MkdirAll(fpath, os.ModePerm)

		} else {

			// Make File
			if err = os.MkdirAll(filepath.Dir(fpath), os.ModePerm); err != nil {
				return filenames, err
			}

			outFile, err := os.OpenFile(fpath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, f.Mode())
			if err != nil {
				return filenames, err
			}

			_, err = io.Copy(outFile, rc)

			// Close the file without defer to close before next iteration of loop
			outFile.Close()

			if err != nil {
				return filenames, err
			}

		}
	}
	return filenames, nil
}

func mimeDeepSniff(readSeeker io.Reader) (string, error) {
	tempFile, err := ioutil.TempFile(os.TempDir(), "mimesniffzip-")

	if err != nil {
		return "", errors.Wrap(err, "mimeDeepSniff, unable to create temp file")
	}

	defer func() {
		tempFile.Close()
		os.Remove(tempFile.Name())
	}()

	_, err = io.Copy(tempFile, readSeeker)

	if err != nil {
		return "", errors.Wrap(err, "mimeDeepSniff, unable to copy temp file")
	}

	dec, err := magicmime.NewDecoder(magicmime.MAGIC_MIME_TYPE | magicmime.MAGIC_ERROR)

	if err != nil {
		return "", errors.Wrap(err, "mimeDeepSniff, unable to create mime decoder")
	}

	defer dec.Close()

	m, err := dec.TypeByFile(tempFile.Name())

	if err != nil {
		return "", errors.Wrap(err, "mimeDeepSniff, unable to get mime type by file")
	}

	return m, nil
}

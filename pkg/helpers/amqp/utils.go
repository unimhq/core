package amqp

import (
	"encoding/json"
	"fmt"

	"github.com/pkg/errors"
	"github.com/streadway/amqp"
)

//BuildJSONMessage builds message with content type = application/json
func BuildJSONMessage(body interface{}) (*amqp.Publishing, error) {
	bodyJSON, err := json.Marshal(body)

	if err != nil {
		return nil, errors.New("ampq.BuildJSONMessage, unable to convert body to JSON")
	}

	msg := amqp.Publishing{
		ContentType: "application/json",
		Body:        bodyJSON,
	}

	return &msg, nil
}

//BuildUniqName build name with uniq postfix
func BuildUniqName(name string, uniq string) string {
	if len(uniq) == 0 {
		return name
	}

	return fmt.Sprintf("%v-%v", name, uniq)
}

func BuildAMQPConnectionUrl(host string, port int, user string, password string, vhost string) (string, error) {
	if len(host) == 0 {
		return "", errors.New("host must be not empty")
	}

	if len(user) == 0 {
		return "", errors.New("user must be not empty")
	}

	if len(password) == 0 {
		return "", errors.New("password must be not empty")
	}

	if port <= 0 || port > 65535 {
		return "", errors.New("port must be between 0 and 65535")
	}

	resultVhost := "/"

	if len(vhost) > 0 {
		resultVhost = vhost
	}

	return fmt.Sprintf("amqp://%s:%s@%s:%v%v", user, password, host, port, resultVhost), nil
}

func BuildRedisConnectionUrl(host string, port int, password string) (string, error) {
	if len(host) == 0 {
		return "", errors.New("host must be not empty")
	}

	if port <= 0 || port > 65535 {
		return "", errors.New("port must be between 0 and 65535")
	}

	if len(password) > 0 {
		return fmt.Sprintf("redis://%s@%s:%v", password, host, port), nil
	}

	return fmt.Sprintf("redis://%s:%v", host, port), nil
}

package api

import (
	localizationHelpers "bitbucket.org/unimhq/core/pkg/helpers/localization"
	strHelper "bitbucket.org/unimhq/core/pkg/helpers/str"
	apiTypes "bitbucket.org/unimhq/core/pkg/types/api"
	coreErrors "bitbucket.org/unimhq/core/pkg/types/errors"
	"encoding/json"
	"fmt"
	"github.com/go-ozzo/ozzo-validation"
	"github.com/nicksnyder/go-i18n/v2/i18n"
	"github.com/pkg/errors"
	"io"
	"reflect"
	"strings"
)

type ParamFieldPreparer func(field reflect.Value) (reflect.Value, error)

func PrepareParams(params interface{}, excludeFieldNames map[string]bool, preparers ...ParamFieldPreparer) error {
	var pV reflect.Value

	if len(preparers) == 0 {
		return nil
	}

	//Если params передан как указатель, берем элемент, на который он ссылается, иначе берем текущий
	if reflect.TypeOf(params).Kind() == reflect.Ptr {
		pV = reflect.ValueOf(params).Elem()
	} else {
		pV = reflect.ValueOf(params)
	}

	switch pV.Kind() {
	case reflect.Struct:
		return prepareStructParams(pV, excludeFieldNames, preparers)
	case reflect.Int:
	case reflect.Int8:
	case reflect.Int16:
	case reflect.Int32:
	case reflect.Int64:
	case reflect.Uint:
	case reflect.Uint8:
	case reflect.Uint16:
	case reflect.Uint32:
	case reflect.Uint64:
	case reflect.Float32:
	case reflect.Float64:
	case reflect.String:
		return prepareSimpleParam(pV, preparers)
	}

	return nil
}

func StringTrimmer(field reflect.Value) (reflect.Value, error) {
	str, ok := field.Interface().(string)

	if ok != true {
		return field, nil
	}

	field.SetString(strings.TrimSpace(str))

	return field, nil
}

func HTMLTagStripper(field reflect.Value) (reflect.Value, error) {
	str, ok := field.Interface().(string)

	if ok != true {
		return field, nil
	}

	field.SetString(strHelper.StripHTMLTags(str))

	return field, nil
}

func StringDeepTrimmer(field reflect.Value) (reflect.Value, error) {
	_, ok := field.Interface().(string)

	if ok == true {
		return StringTrimmer(field)
	}

	if field.Kind() == reflect.Struct {
		for i := 0; i < field.NumField(); i++ {
			f := field.Field(i)

			if f.Kind() == reflect.Struct {
				return StringDeepTrimmer(f)
			}

			//Если тип текущего поля - указатель, берем значение, на которое он ссылается, иначе используем текущее поле
			if f.Kind() == reflect.Ptr {
				if f.IsNil() {
					continue
				}

				f = f.Elem()
			}

			//Берем название поля
			fieldName := field.Type().Field(i).Name

			//Если поле не изменяемое (передана не ссылка в params или тип поля не явялется ссылкой)
			if f.CanSet() != true {
				return f, errors.New(fmt.Sprintf("unable to trim spaces for field: %v, value is not addressable", fieldName))
			}

			return StringTrimmer(f)
		}
	}
	return field, nil
}

func MustDecodeParams(reader io.Reader, to interface{}) {
	bodyDecoder := json.NewDecoder(reader)

	err := bodyDecoder.Decode(&to)

	if err != nil {
		panic(apiTypes.NewWrongApiParams(errors.Wrap(err, "params.MustDecodeParams, unable to parse json params")))
	}
}

func MustPrepareParams(p apiTypes.Params) apiTypes.Params {
	err := p.Prepare()

	if err != nil {
		panic(errors.Wrap(err, "params.MustPrepareParams, unable to prepare params"))
	}

	return p
}

func MustValidateParams(validatable apiTypes.Validatable) error {
	validationErrors := validatable.Validate()

	if internalError, ok := validationErrors.(validation.InternalError); ok == true {
		panic(errors.Wrap(internalError, "params.MustValidateParams, unable to validate params"))
	}

	return validationErrors
}

func MustValidateParamsLocalized(validatable apiTypes.Validatable, localizer *i18n.Localizer) error {
	validationErrors := validatable.Validate()

	if internalError, ok := validationErrors.(validation.InternalError); ok == true {
		panic(errors.Wrap(internalError, "params.MustValidateParams, unable to validate params"))
	}

	validationErrorsAsMap, ok := validationErrors.(validation.Errors)

	if ok != true {
		return validationErrors
	}

	resultValidationErrors := make(map[string]error)
	for field, err := range validationErrorsAsMap {
		publicError, ok := err.(*coreErrors.Public)
		if ok != true {
			resultValidationErrors[field] = err
			continue
		}

		resultValidationErrors[field] = errors.New(localizationHelpers.Localize(localizer, publicError.MessageID, nil, nil))
	}

	return validation.Errors(resultValidationErrors)
}

// Field preparers

var preparableType = reflect.TypeOf((*apiTypes.Preparable)(nil)).Elem()

func prepareStructParams(value reflect.Value, excludeFieldNames map[string]bool, preparers []ParamFieldPreparer) error {
	var err error = nil

	//Бежим по всем полям структуры
	for i := 0; i < value.NumField(); i++ {
		field := value.Field(i)

		//Если тип текущего поля - указатель, берем значение, на которое он ссылается, иначе используем текущее поле
		if field.Kind() == reflect.Ptr {
			if field.IsNil() {
				continue
			}

			field = field.Elem()
		}

		if field.CanAddr() && field.Addr().Type().Implements(preparableType) {
			if err := field.Addr().Interface().(apiTypes.Preparable).Prepare(); err != nil {
				return err
			}
		}

		if field.Type().Implements(preparableType) {
			if err := field.Interface().(apiTypes.Preparable).Prepare(); err != nil {
				return err
			}
		}

		//Берем название поля
		fieldName := value.Type().Field(i).Name

		//Если поле добавлено в исключения, ничего не делаем
		if _, ok := excludeFieldNames[fieldName]; ok == true {
			continue
		}

		_, hasJSONTag := value.Type().Field(i).Tag.Lookup("json")

		if hasJSONTag == false {
			continue
		}

		//Если поле не изменяемое (передана не ссылка в params или тип поля не явялется ссылкой)
		if field.CanSet() != true {
			return errors.New(fmt.Sprintf("unable to prepare struct field: %v, value is not addressable", fieldName))
		}

		for _, preparer := range preparers {
			field, err = preparer(field)

			if err != nil {
				return errors.Wrap(err, "api.prepareStructParams, unable to apply preparer")
			}
		}
	}

	return nil
}

func prepareSimpleParam(value reflect.Value, preparers []ParamFieldPreparer) error {
	var err error = nil

	//Если поле не изменяемое (передана не ссылка в params или тип поля не явялется ссылкой)
	if value.CanSet() != true {
		return errors.New("unable to prepare simple value, value is not addressable")
	}

	for _, preparer := range preparers {
		value, err = preparer(value)

		if err != nil {
			return errors.Wrap(err, "api.prepareSimpleParam, unable to apply preparer")
		}
	}

	return nil
}

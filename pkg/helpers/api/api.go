package api

import (
	apiTypes "bitbucket.org/unimhq/core/pkg/types/api"
	"encoding/json"
	"fmt"
	v "github.com/go-ozzo/ozzo-validation"
	"io"
)

//ErrorResponse is helper for fast error response creation
func ErrorResponse(error interface{}, code apiTypes.ErrorCode) *apiTypes.Response {
	return &apiTypes.Response{
		Success:   false,
		Data:      nil,
		Error:     error,
		ErrorCode: code,
	}
}

//ErrorResponseWithData is helper for fast error response creation
func ErrorResponseWithData(error interface{}, code apiTypes.ErrorCode, data interface{}) *apiTypes.Response {
	return &apiTypes.Response{
		Success:   false,
		Data:      data,
		Error:     error,
		ErrorCode: code,
	}
}

//InternalServerError is helper for fast server error response creation
func InternalServerError() *apiTypes.Response {
	return ErrorResponseByCode(apiTypes.InternalServerErrorCode)
}

//MalformedParamsError is helper for fast params error response creation
func MalformedParamsError() *apiTypes.Response {
	return ErrorResponseByCode(apiTypes.MalformedParamsErrorCode)
}

func ErrorResponseByCode(code apiTypes.ErrorCode) *apiTypes.Response {
	message, ok := apiTypes.ErrorMessagesByCode[code]

	if ok != true {
		return ErrorResponse(fmt.Sprintf("Error with code: %v", code), code)
	}

	return ErrorResponse(message, code)
}

//SuccessResponse is helper for fast success response creation
func SuccessResponse(data interface{}) *apiTypes.Response {
	return &apiTypes.Response{
		Success: true,
		Error:   nil,
		Data:    data,
	}
}

func EmptySuccessResponse() *apiTypes.Response {
	return SuccessResponse(apiTypes.EmptySuccess{})
}

//ToJSON Encode response to json and send to writer
func ToJSON(w io.Writer, response apiTypes.Response) error {
	enc := json.NewEncoder(w)
	enc.SetEscapeHTML(false)

	return enc.Encode(response)
}

func ValidationErrorsByFields(validationErrors v.Errors) v.Errors {
	errorsFiltered := validationErrors.Filter()

	if errorsFiltered != nil {
		errorsByFields := errorsFiltered.(v.Errors)

		if len(errorsByFields) != 0 {
			return errorsByFields
		}
	}

	return nil
}

package api

import (
	"bytes"
	"encoding/json"
	"github.com/pkg/errors"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"io/ioutil"
	"net/http"
)

const BodySymbolsLimit = 20000

func Log(req *http.Request, requestLogger *zap.Logger, message string) (err error) {
	buf, _ := ioutil.ReadAll(req.Body)
	toLog := ioutil.NopCloser(bytes.NewBuffer(buf))
	ToNext := ioutil.NopCloser(bytes.NewBuffer(buf))

	b, err := ioutil.ReadAll(toLog)

	if err != nil {
		err = errors.Wrap(err, "api.Log, unable to read bytes from request")
		b = []byte{}
	}

	loggerFields := make([]zapcore.Field, 0, 0)

	loggerFields = append(loggerFields,
		zap.String("method", req.Method),
		zap.String("remoteAddr", req.RemoteAddr),
		zap.String("host", req.Host),
		zap.String("requestUri", req.RequestURI),
		zap.Int64("contentLength", req.ContentLength),
		zap.Any("headers", req.Header))

	if req.URL != nil {
		loggerFields = append(loggerFields, zap.String("url", req.URL.String()))
	}

	if len(b) <= BodySymbolsLimit {
		if req.Header.Get("Content-Type") == "application/json" {
			var field = zapcore.Field{}
			if true == json.Valid(b) {
				rawMessage := json.RawMessage(b)
				field = zap.Any("body", &rawMessage)
			} else {
				field = zap.ByteString("body", b)
			}
			loggerFields = append(loggerFields, field)
		} else {
			loggerFields = append(loggerFields, zap.ByteString("body", b))
		}
	}

	req.Body = ToNext
	requestLogger.Info(message, loggerFields...)

	return err
}

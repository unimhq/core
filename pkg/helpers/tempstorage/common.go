package tempstorage

import (
	"bitbucket.org/unimhq/core/pkg/types"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"reflect"
	"runtime"
	"strings"
)

func BuildAppDumpReadyKey(appType types.App) string {
	return fmt.Sprintf("dump-ready-for-%v", appType)
}

func BuildMccLastUpdatedDateKey() string {
	return "mcc-last-updated-date"
}

func BuildKeyByParams(prefix string, params ...interface{}) string {
	stringParams := make([]string, 0, len(params))

	for _, param := range params {
		if param == nil {
			continue
		}

		if param == nil {
			continue
		}

		resultParam := param

		if reflect.TypeOf(param).Kind() == reflect.Ptr {
			if reflect.ValueOf(param).IsNil() == true {
				continue
			}

			resultParam = fmt.Sprint(reflect.ValueOf(param).Elem())
		}

		stringParam := strings.Replace(fmt.Sprint(resultParam), " ", "", -1)

		if len(stringParam) == 0 {
			continue
		}

		stringParams = append(stringParams, stringParam)
	}

	resultPrefix := prefix

	if len(resultPrefix) == 0 {
		pc, _, _, _ := runtime.Caller(1)
		funcObj := runtime.FuncForPC(pc)

		hasher := sha256.New()
		_, err := hasher.Write([]byte(funcObj.Name()))

		if err != nil {
			panic(fmt.Sprintf("tempstorage.BuildKeyByParams, unable to hash func name: %s", funcObj.Name()))
		}

		resultPrefix = fmt.Sprintf("temp-%s", hex.EncodeToString(hasher.Sum(nil)))
	}

	return fmt.Sprintf("%s-%s", resultPrefix, strings.Join(stringParams, "-"))
}

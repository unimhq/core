package jwt

import (
	apiHelpers "bitbucket.org/unimhq/core/pkg/helpers/api"
	apiTypes "bitbucket.org/unimhq/core/pkg/types/api"
	jwtTypes "bitbucket.org/unimhq/core/pkg/types/jwt"
	"github.com/dgrijalva/jwt-go"
)

func TokenErrorResponse(err error) *apiTypes.Response {
	if err == nil {
		return nil
	}

	var response *apiTypes.Response

	switch err.(type) {
	case *jwtTypes.AuthHeaderFormatError:
		response = apiHelpers.ErrorResponseByCode(apiTypes.JwtAuthHeaderWrongFormatErrorCode)
	case *jwtTypes.TokenNotFoundError:
		response = apiHelpers.ErrorResponseByCode(apiTypes.JwtTokenNotFoundErrorCode)
	case *jwtTypes.TokenAlgValidationError:
		response = apiHelpers.ErrorResponseByCode(apiTypes.JwtTokenAlgValidationErrorCode)
	case *jwtTypes.TokenValidationError:
		response = apiHelpers.ErrorResponseByCode(apiTypes.JwtTokenValidationErrorCode)
	case *jwt.ValidationError:
		errs := err.(*jwt.ValidationError).Errors

		if errs&jwt.ValidationErrorSignatureInvalid > 0 {
			response = apiHelpers.ErrorResponseByCode(apiTypes.JwtTokenInvalidSignatureErrorCode)
		} else if errs&jwt.ValidationErrorMalformed > 0 {
			response = apiHelpers.ErrorResponseByCode(apiTypes.JwtTokenMalformedErrorCode)
		} else if errs&jwt.ValidationErrorUnverifiable > 0 {
			response = apiHelpers.ErrorResponseByCode(apiTypes.JwtTokenUnverifiableErrorCode)
		} else if errs&jwt.ValidationErrorExpired > 0 {
			response = apiHelpers.ErrorResponseByCode(apiTypes.JwtTokenExpiredErrorCode)
		} else if errs&jwt.ValidationErrorIssuedAt > 0 {
			response = apiHelpers.ErrorResponseByCode(apiTypes.JwtTokenNotIssuedErrorCode)
		} else if errs&jwt.ValidationErrorNotValidYet > 0 {
			response = apiHelpers.ErrorResponseByCode(apiTypes.JwtTokenNotValidYetErrorCode)
		} else {
			//Common token validation error
			response = apiHelpers.ErrorResponseByCode(apiTypes.JwtTokenValidationErrorCode)
		}
	default:
		response = apiHelpers.ErrorResponseByCode(apiTypes.JwtAuthorizationErrorCode)
	}

	return response
}

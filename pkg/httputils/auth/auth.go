package auth

import (
	"context"
	"net/http"
)

type Authenticator interface {
	Authenticate(ctx context.Context, request *http.Request) error
}

type noOpAuthenticator struct {
}

func (o *noOpAuthenticator) Authenticate(ctx context.Context, request *http.Request) error {
	return nil
}

var NoOpAuthenticator Authenticator = &noOpAuthenticator{}

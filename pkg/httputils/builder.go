package httputils

import (
	"context"
	"fmt"
	"github.com/asaskevich/govalidator"
	"github.com/pkg/errors"
	"net/http"
	"net/url"
	"strings"
)

type RequestCustomizer func(ctx context.Context, req *http.Request) error

func getFullUrl(baseUrl string, path string) string {
	return fmt.Sprintf("%s%s", baseUrl, path)
}

func verifyFullUrl(baseUrl string, path string) error {
	if !govalidator.IsURL(baseUrl) {
		return errors.New(fmt.Sprintf("baseUrl is not url `%q`", baseUrl))
	}

	if len(path) == 0 {
		return errors.New("path can't be empty")
	}

	fullUrl := getFullUrl(baseUrl, path)
	if !govalidator.IsURL(fullUrl) {
		return errors.New(fmt.Sprintf("fullUrl is not url `%q`", fullUrl))
	}

	return nil
}

func buildUrl(
	baseUrl string,
	path string,
	queryParameters map[string]string,
	pathParameters map[string]string,
) (*string, error) {
	err := verifyFullUrl(baseUrl, path)
	if err != nil {
		return nil, err
	}

	fullUrl := getFullUrl(baseUrl, path)
	u, err := url.Parse(fullUrl)
	if err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("Wrong url %q", fullUrl))
	}
	q, err := url.ParseQuery(u.RawQuery)
	if err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("Wrong url %q", fullUrl))
	}

	// add query parameters
	for name, value := range queryParameters {
		q.Add(name, value)
	}
	u.RawQuery = q.Encode()

	result := u.String()

	// substitute path parameters
	for name, value := range pathParameters {
		result = strings.ReplaceAll(result, "{"+name+"}", value)
	}

	if strings.Contains(result, "{") || strings.Contains(result, "}") {
		return nil, errors.New(fmt.Sprintf("Some path parameters not set %q", result))
	}

	return &result, nil
}

func applyCustomizers(
	ctx context.Context,
	httpRequest *http.Request,
	requestCustomizers []RequestCustomizer,
) error {
	for _, requestCustomizer := range requestCustomizers {
		err := requestCustomizer(ctx, httpRequest)
		if err != nil {
			return errors.Wrap(err, fmt.Sprintf("Can't apply requestCustomizer to request %v", httpRequest))
		}
	}
	return nil
}

func copyStringMap(src map[string]string) map[string]string {
	dst := make(map[string]string)
	for key, value := range src {
		dst[key] = value
	}

	return dst
}

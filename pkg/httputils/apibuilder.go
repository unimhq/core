package httputils

import "bitbucket.org/unimhq/core/pkg/httputils/auth"

type Api struct {
	baseUrl            string
	authenticator      auth.Authenticator
	requestCustomizers []RequestCustomizer
	httpCaller         HttpCaller
}

func NewApi(baseUrl string) Api {
	return Api{
		baseUrl: baseUrl,
	}
}

func (api *Api) AddCustomizer(requestCustomizer RequestCustomizer) *Api {
	newApi := *api //create copy
	newApi.requestCustomizers = append(newApi.requestCustomizers, requestCustomizer)
	return &newApi
}

func (api *Api) WithTracing() *Api {
	return api.AddCustomizer(TraceCustomizer)
}

func (api *Api) WithLogging() *Api {
	return api.AddCustomizer(LoggerCustomizer)
}

func (api *Api) WithAuthenticator(authenticator auth.Authenticator) *Api {
	newApi := *api //create copy
	newApi.authenticator = authenticator
	return &newApi
}

func (api *Api) WithCaller(httpCaller HttpCaller) *Api {
	newApi := *api //create copy
	newApi.httpCaller = httpCaller
	return &newApi
}

func (api *Api) Post(path string) *PostBuilder {
	return &PostBuilder{
		baseUrl: api.baseUrl,
		path:    path,

		requestCustomizers: api.requestCustomizers,
		httpCaller:         api.httpCaller,
		queryParameters:    make(map[string]string),
		pathParameters:     make(map[string]string),
	}
}

func (api *Api) Get(path string) *GetBuilder {
	return &GetBuilder{
		baseUrl: api.baseUrl,
		path:    path,

		requestCustomizers: api.requestCustomizers,
		httpCaller:         api.httpCaller,
		queryParameters:    make(map[string]string),
		pathParameters:     make(map[string]string),
	}
}

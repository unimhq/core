package httputils

import (
	"context"
	"encoding/json"
	"github.com/pkg/errors"
	"io"
	"io/ioutil"
	"net/http"
	"reflect"
)

func createToJsonResponseProcessor(resultRef interface{}) (ResponseProcessor, error) {
	v := reflect.ValueOf(resultRef)
	if v.Kind() != reflect.Ptr {
		return nil, errors.New("must pass a pointer, not a value, to createToJsonResponseProcessor destination")
	}
	if v.IsNil() {
		return nil, errors.New("nil pointer passed to createToJsonResponseProcessor destination")
	}

	return func(ctx context.Context, response *http.Response) (interface{}, error) {
		buf, err := ioutil.ReadAll(response.Body)
		if err != nil {
			return nil, errors.Wrap(err, "can't read all")
		}

		err = json.Unmarshal(buf, &resultRef)

		return resultRef, nil
	}, nil
}

// Hop-by-hop headers. These are removed when sent to the backend.
// http://www.w3.org/Protocols/rfc2616/rfc2616-sec13.html
var hopHeaders = []string{
	"Connection",
	"Keep-Alive",
	"Proxy-Authenticate",
	"Proxy-Authorization",
	"Te", // canonicalized version of "TE"
	"Trailers",
	"Transfer-Encoding",
	"Upgrade",
}

func delHopHeaders(header http.Header) {
	for _, h := range hopHeaders {
		header.Del(h)
	}
}

func copyHeader(dst, src http.Header) {
	for k, vv := range src {
		for _, v := range vv {
			dst.Add(k, v)
		}
	}
}

func createProxyProcessor(responseWriter http.ResponseWriter) (ResponseProcessor, error) {
	if responseWriter == nil {
		return nil, errors.New("responseWriter can't be nil")
	}

	return func(ctx context.Context, response *http.Response) (interface{}, error) {
		delHopHeaders(response.Header)

		copyHeader(responseWriter.Header(), response.Header)
		responseWriter.WriteHeader(response.StatusCode)
		_, err := io.Copy(responseWriter, response.Body)

		if err != nil {
			return nil, errors.Wrap(err, "can't copy boyd")
		}

		return nil, nil
	}, nil
}

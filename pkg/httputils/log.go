package httputils

import "go.uber.org/zap"

func logger() *zap.Logger {
	return zap.L().Named("httputils")
}

func slogger() *zap.SugaredLogger {
	return zap.S().Named("httputils")
}

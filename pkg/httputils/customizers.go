package httputils

import (
	coreApiHelpers "bitbucket.org/unimhq/core/pkg/helpers/api"
	"context"
	"github.com/opentracing/opentracing-go"
	"github.com/pkg/errors"
	"net/http"
)

func LoggerCustomizer(ctx context.Context, req *http.Request) error {
	err := coreApiHelpers.Log(req, logger(), "Run call")
	if err != nil {
		return errors.Wrap(err, "can't log request")
	}

	return nil
}

func TraceCustomizer(ctx context.Context, req *http.Request) error {
	if span := opentracing.SpanFromContext(ctx); span != nil {
		err := opentracing.GlobalTracer().Inject(
			span.Context(),
			opentracing.HTTPHeaders,
			opentracing.HTTPHeadersCarrier(req.Header))

		if err != nil {
			return errors.Wrap(err, "can't inject trace info")
		}
	}

	return nil
}

package httputils

import (
	coreApiHelpers "bitbucket.org/unimhq/core/pkg/helpers/api"
	"context"
	"fmt"
	"github.com/pkg/errors"
	"net/http"
	"time"
)

type ResponseProcessor func(ctx context.Context, response *http.Response) (interface{}, error)

var DefaultHttpCaller HttpCaller

func init() {
	var err error
	DefaultHttpCaller, err = NewHttpCaller()
	if err != nil {
		panic(err)
	}
}

type HttpCaller interface {
	/*
		helper for consolidation of all http requests to external systems
	*/
	Do(ctx context.Context, req *http.Request, responseProcessor ResponseProcessor) (interface{}, error)

	GetJson(ctx context.Context, req *http.Request, resultRef interface{}) error

	Proxy(ctx context.Context, req *http.Request, w http.ResponseWriter) error
}

func NewHttpCaller() (HttpCaller, error) {
	const defaultTimeout = 5
	return &httpCaller{
		timeout:            defaultTimeout,
		requestCustomizers: make([]RequestCustomizer, 0),
	}, nil
}

type httpCaller struct {
	timeout time.Duration

	requestCustomizers []RequestCustomizer
}

func (h *httpCaller) AddCustomizer(requestCustomizer RequestCustomizer) *httpCaller {
	h.requestCustomizers = append(h.requestCustomizers, requestCustomizer)
	return h
}

func (h *httpCaller) WithTracing() *httpCaller {
	return h.AddCustomizer(TraceCustomizer)
}

func (h *httpCaller) WithLogging() *httpCaller {
	return h.AddCustomizer(LoggerCustomizer)
}

func (h *httpCaller) Do(ctx context.Context, req *http.Request, responseProcessor ResponseProcessor) (interface{}, error) {
	slogger().Debugw("Perform call ", "url", req.URL)

	err := applyCustomizers(ctx, req, h.requestCustomizers)
	if err != nil {
		return nil, errors.Wrap(err, "Can't apply customizers")
	}

	httpClient := http.Client{
		Timeout: time.Second * h.timeout,
	}

	defer httpClient.CloseIdleConnections()

	coreApiHelpers.Log(req, logger(), "Run call")
	response, err := httpClient.Do(req)
	if err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("can't perform http request %v", req))
	}

	defer func() {
		if response == nil {
			return
		}

		err := response.Body.Close()
		if err != nil {
			slogger().Warnw("Can't close body for success response", "url", req.URL)
		}
	}()

	if response.StatusCode < 200 || response.StatusCode >= 300 {
		return nil, errors.New(fmt.Sprintf("failed http call, status code: %v", response.StatusCode))
	}

	result, err := responseProcessor(ctx, response)
	if err != nil {
		return nil, errors.Wrap(err, "can't process response")
	}

	return result, nil
}

func (h *httpCaller) GetJson(ctx context.Context, req *http.Request, resultRef interface{}) error {
	responseProcessor, err := createToJsonResponseProcessor(resultRef)
	if err != nil {
		return errors.Wrap(err, "Can't create responseProcessor")
	}

	_, err = h.Do(ctx, req, responseProcessor)
	if err != nil {
		return errors.Wrap(err, "Operation failed")
	}

	return nil
}

func (h *httpCaller) Proxy(ctx context.Context, req *http.Request, w http.ResponseWriter) error {
	responseProcessor, err := createProxyProcessor(w)
	if err != nil {
		return errors.Wrap(err, "Can't create responseProcessor")
	}

	_, err = h.Do(ctx, req, responseProcessor)
	if err != nil {
		return errors.Wrap(err, "Operation failed")
	}

	return nil
}

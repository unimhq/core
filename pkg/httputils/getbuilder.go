package httputils

import (
	"bitbucket.org/unimhq/core/pkg/httputils/auth"
	"bytes"
	"context"
	"github.com/pkg/errors"
	"net/http"
)

func NewGet(baseUrl string, path string) *GetBuilder {
	return &GetBuilder{
		baseUrl: baseUrl,
		path:    path,

		requestCustomizers: make([]RequestCustomizer, 0),
		httpCaller:         DefaultHttpCaller,
		queryParameters:    make(map[string]string),
		pathParameters:     make(map[string]string),
	}
}

type GetBuilder struct {
	baseUrl string
	path    string

	authenticator      auth.Authenticator
	requestCustomizers []RequestCustomizer
	httpCaller         HttpCaller
	queryParameters    map[string]string
	pathParameters     map[string]string
}

func (b *GetBuilder) AddCustomizer(requestCustomizer RequestCustomizer) *GetBuilder {
	newBuilder := *b //create copy
	newBuilder.requestCustomizers = append(newBuilder.requestCustomizers, requestCustomizer)
	return &newBuilder
}

func (b *GetBuilder) WithTracing() *GetBuilder {
	return b.AddCustomizer(TraceCustomizer)
}

func (b *GetBuilder) WithLogging() *GetBuilder {
	return b.AddCustomizer(LoggerCustomizer)
}

func (b *GetBuilder) WithAuthenticator(authenticator auth.Authenticator) *GetBuilder {
	newBuilder := *b //create copy
	newBuilder.authenticator = authenticator
	return &newBuilder
}

func (b *GetBuilder) WithCaller(httpCaller HttpCaller) *GetBuilder {
	newBuilder := *b //create copy
	newBuilder.httpCaller = httpCaller
	return &newBuilder
}

func (b *GetBuilder) AddQueryParameter(key string, value string) *GetBuilder {
	newQueryParameters := copyStringMap(b.queryParameters)
	newQueryParameters[key] = value

	newBuilder := *b //create copy
	newBuilder.queryParameters = newQueryParameters
	return &newBuilder
}

func (b *GetBuilder) AddPathParameter(key string, value string) *GetBuilder {
	newPathParameters := copyStringMap(b.pathParameters)
	newPathParameters[key] = value

	newBuilder := *b //create copy
	newBuilder.pathParameters = newPathParameters
	return &newBuilder
}

/**
check baseUrl, path, fullUrl
*/
func (b *GetBuilder) Verify() error {
	return verifyFullUrl(b.baseUrl, b.path)
}

func (b *GetBuilder) createHttpRequest(ctx context.Context) (*http.Request, error) {
	fullUrl, err := buildUrl(b.baseUrl, b.path, b.queryParameters, b.pathParameters)
	if err != nil {
		return nil, errors.Wrap(err, "Can't build full url")
	}

	httpRequest, err := http.NewRequest("GET", *fullUrl, bytes.NewBuffer([]byte{}))
	if err != nil {
		return nil, errors.Wrap(err, "Can't create httpRequest")
	}

	err = applyCustomizers(ctx, httpRequest, b.requestCustomizers)
	if err != nil {
		return nil, errors.Wrap(err, "Can't apply customizers")
	}

	if b.authenticator != nil {
		err := b.authenticator.Authenticate(ctx, httpRequest)
		if err != nil {
			return nil, errors.Wrap(err, "can't perform authentication")
		}
	}

	return httpRequest, nil
}

func (b *GetBuilder) Run(ctx context.Context, responseProcessor ResponseProcessor) (interface{}, error) {
	httpRequest, err := b.createHttpRequest(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "Can't create http.request")
	}

	return DefaultHttpCaller.Do(ctx, httpRequest, responseProcessor)
}

func (b *GetBuilder) GetJson(ctx context.Context, resultRef interface{}) error {
	httpRequest, err := b.createHttpRequest(ctx)
	if err != nil {
		return errors.Wrap(err, "Can't create http.request")
	}

	err = b.httpCaller.GetJson(ctx, httpRequest, resultRef)
	if err != nil {
		return errors.Wrap(err, "Can't perform operation")
	}

	return nil
}

func (b *GetBuilder) Proxy(ctx context.Context, w http.ResponseWriter) error {
	httpRequest, err := b.createHttpRequest(ctx)
	if err != nil {
		return errors.Wrap(err, "Can't create http.request")
	}

	err = b.httpCaller.Proxy(ctx, httpRequest, w)
	if err != nil {
		return errors.Wrap(err, "Can't perform operation")
	}

	return nil
}

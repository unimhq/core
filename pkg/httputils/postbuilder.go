package httputils

import (
	"bitbucket.org/unimhq/core/pkg/httputils/auth"
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"github.com/pkg/errors"
	"io"
	"net/http"
)

func NewPost(baseUrl string, path string) *PostBuilder {
	return &PostBuilder{
		baseUrl: baseUrl,
		path:    path,

		requestCustomizers: make([]RequestCustomizer, 0),
		httpCaller:         DefaultHttpCaller,
		queryParameters:    make(map[string]string),
		pathParameters:     make(map[string]string),
	}
}

type PostBuilder struct {
	baseUrl string
	path    string

	authenticator      auth.Authenticator
	requestCustomizers []RequestCustomizer
	httpCaller         HttpCaller

	queryParameters map[string]string
	pathParameters  map[string]string

	payload interface{} // serializable to json
}

func (b *PostBuilder) AddCustomizer(requestCustomizer RequestCustomizer) *PostBuilder {
	newBuilder := *b //create copy
	newBuilder.requestCustomizers = append(b.requestCustomizers, requestCustomizer)
	return &newBuilder
}

func (b *PostBuilder) WithTracing() *PostBuilder {
	return b.AddCustomizer(TraceCustomizer)
}

func (b *PostBuilder) WithLogging() *PostBuilder {
	return b.AddCustomizer(LoggerCustomizer)
}

func (b *PostBuilder) WithAuthenticator(authenticator auth.Authenticator) *PostBuilder {
	newBuilder := *b //create copy
	newBuilder.authenticator = authenticator
	return &newBuilder
}

func (b *PostBuilder) WithPayload(payload interface{}) *PostBuilder {
	newBuilder := *b //create copy
	newBuilder.payload = payload
	return &newBuilder
}

func (b *PostBuilder) AddQueryParameter(key string, value string) *PostBuilder {
	newQueryParameters := copyStringMap(b.queryParameters)
	newQueryParameters[key] = value

	newBuilder := *b //create copy
	newBuilder.queryParameters = newQueryParameters
	return &newBuilder
}

func (b *PostBuilder) AddPathParameter(key string, value string) *PostBuilder {
	newPathParameters := copyStringMap(b.pathParameters)
	newPathParameters[key] = value

	newBuilder := *b //create copy
	newBuilder.pathParameters = newPathParameters
	return &newBuilder
}

func (b *PostBuilder) WithCaller(httpCaller HttpCaller) *PostBuilder {
	newBuilder := *b //create copy
	newBuilder.httpCaller = httpCaller
	return &newBuilder
}

/**
check baseUrl, path, fullUrl
*/
func (b *PostBuilder) Verify() error {
	return verifyFullUrl(b.baseUrl, b.path)
}

func (b *PostBuilder) createHttpRequest(ctx context.Context) (*http.Request, error) {
	fullUrl, err := buildUrl(b.baseUrl, b.path, b.queryParameters, b.pathParameters)
	if err != nil {
		return nil, errors.Wrap(err, "Can't build full url")
	}

	var requestBody io.Reader = nil
	if b.payload != nil {
		payloadAsJson, err := json.Marshal(b.payload)
		if err != nil {
			return nil, errors.Wrap(err, fmt.Sprintf("can't serialize to json, %v", b.payload))
		}
		requestBody = bytes.NewBuffer(payloadAsJson)
	} else {
		requestBody = bytes.NewBuffer([]byte{})
	}

	httpRequest, err := http.NewRequest("POST", *fullUrl, requestBody)
	if err != nil {
		return nil, errors.Wrap(err, "Can't create httpRequest")
	}
	if requestBody != nil { // json payload set
		httpRequest.Header.Add("Content-Type", "application/json")
	}

	err = applyCustomizers(ctx, httpRequest, b.requestCustomizers)
	if err != nil {
		return nil, errors.Wrap(err, "Can't apply customizers")
	}

	if b.authenticator != nil {
		err := b.authenticator.Authenticate(ctx, httpRequest)
		if err != nil {
			return nil, errors.Wrap(err, "can't perform authentication")
		}
	}

	return httpRequest, nil
}

func (b *PostBuilder) Run(ctx context.Context, responseProcessor ResponseProcessor) (interface{}, error) {
	httpRequest, err := b.createHttpRequest(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "Can't create http.request")
	}

	return DefaultHttpCaller.Do(ctx, httpRequest, responseProcessor)
}

func (b *PostBuilder) GetJson(ctx context.Context, resultRef interface{}) error {
	httpRequest, err := b.createHttpRequest(ctx)
	if err != nil {
		return errors.Wrap(err, "Can't create http.request")
	}

	err = b.httpCaller.GetJson(ctx, httpRequest, resultRef)
	if err != nil {
		return errors.Wrap(err, "Can't perform operation")
	}

	return nil
}

func (b *PostBuilder) Proxy(ctx context.Context, w http.ResponseWriter) error {
	httpRequest, err := b.createHttpRequest(ctx)
	if err != nil {
		return errors.Wrap(err, "Can't create http.request")
	}

	err = b.httpCaller.Proxy(ctx, httpRequest, w)
	if err != nil {
		return errors.Wrap(err, "Can't perform operation")
	}

	return nil
}

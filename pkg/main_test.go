package pkg__test

import (
	_ "bitbucket.org/unimhq/core/pkg/api"
	_ "bitbucket.org/unimhq/core/pkg/api/middlewares"
	_ "bitbucket.org/unimhq/core/pkg/helpers/amqp"
	_ "bitbucket.org/unimhq/core/pkg/helpers/api"
	_ "bitbucket.org/unimhq/core/pkg/helpers/auth"
	_ "bitbucket.org/unimhq/core/pkg/helpers/cmd"
	_ "bitbucket.org/unimhq/core/pkg/helpers/currency"
	_ "bitbucket.org/unimhq/core/pkg/helpers/db"
	_ "bitbucket.org/unimhq/core/pkg/helpers/file"
	_ "bitbucket.org/unimhq/core/pkg/helpers/jwt"
	_ "bitbucket.org/unimhq/core/pkg/helpers/localization"
	_ "bitbucket.org/unimhq/core/pkg/helpers/str"
	_ "bitbucket.org/unimhq/core/pkg/helpers/tempstorage"
	_ "bitbucket.org/unimhq/core/pkg/helpers/timeformat"
	_ "bitbucket.org/unimhq/core/pkg/helpers/validation"
	_ "bitbucket.org/unimhq/core/pkg/services"
	_ "bitbucket.org/unimhq/core/pkg/services/amqp"
	_ "bitbucket.org/unimhq/core/pkg/services/redis"
	_ "bitbucket.org/unimhq/core/pkg/services/storage"
	_ "bitbucket.org/unimhq/core/pkg/services/tasks"
	_ "bitbucket.org/unimhq/core/pkg/third_party/machinerylogs"
	_ "bitbucket.org/unimhq/core/pkg/third_party/zapsentry"
	_ "bitbucket.org/unimhq/core/pkg/types"
	_ "bitbucket.org/unimhq/core/pkg/types/amqpmessages"
	_ "bitbucket.org/unimhq/core/pkg/types/api"
	_ "bitbucket.org/unimhq/core/pkg/types/errors"
	_ "bitbucket.org/unimhq/core/pkg/types/exchange"
	_ "bitbucket.org/unimhq/core/pkg/types/exchangeroutingkeys"
	_ "bitbucket.org/unimhq/core/pkg/types/jwt"
	_ "bitbucket.org/unimhq/core/pkg/types/sortorders"
	"testing"
)

// surrogate test for check that all imports can be compiled
func TestImports(t *testing.T) {
}

package jwt

import (
	"github.com/dgrijalva/jwt-go"
)

//UnimClaims extends jwt.StandartClaims
type UnimClaims struct {
	jwt.StandardClaims
	Version  int64  `json:"ver,omitempty"`
	UserID   string `json:"uid,omitempty"`
	ClientID string `json:"cid,omitempty"`
}

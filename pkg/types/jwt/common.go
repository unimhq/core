package jwt

//AuthHeaderFormatError
type AuthHeaderFormatError struct {
	Err error
}

func (e *AuthHeaderFormatError) Error() string {
	return e.Err.Error()
}

func NewAuthHeaderFormatError(err error) AuthHeaderFormatError {
	return AuthHeaderFormatError{
		Err: err,
	}
}

//TokenNotFoundError
type TokenNotFoundError struct {
	Err error
}

func (e *TokenNotFoundError) Error() string {
	return e.Err.Error()
}

func NewTokenNotFoundError(err error) TokenNotFoundError {
	return TokenNotFoundError{
		Err: err,
	}
}

//TokenAlgValidationError
type TokenAlgValidationError struct {
	Err error
}

func (e *TokenAlgValidationError) Error() string {
	return e.Err.Error()
}

func NewTokenAlgValidationError(err error) TokenAlgValidationError {
	return TokenAlgValidationError{
		Err: err,
	}
}

//TokenValidationError
type TokenValidationError struct {
	Err error
}

func (e *TokenValidationError) Error() string {
	return e.Err.Error()
}

func NewTokenValidationError(err error) TokenValidationError {
	return TokenValidationError{
		Err: err,
	}
}

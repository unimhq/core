package exchangeroutingkeys

import "bitbucket.org/unimhq/core/pkg/types"

const (
	CasesMccStatusChange types.AmqpRoutingKey = "cases.mcc.status.change"
)

package exchange

import "bitbucket.org/unimhq/core/pkg/types"

const (
	CasesEventBus types.AmqpExchangeName = "casesEventBus"
)

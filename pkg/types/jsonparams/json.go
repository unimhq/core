package jsonparams

import (
	apiHelpers "bitbucket.org/unimhq/core/pkg/helpers/api"
	"encoding/json"
	v "github.com/go-ozzo/ozzo-validation"
	"time"
)

func extractError(err error) error {
	if err == nil {
		return err
	}

	errorsFiltered := err.(v.Errors).Filter()

	if errorsFiltered != nil {
		errorsByFields := errorsFiltered.(v.Errors)

		for _, e := range errorsByFields {
			return e
		}
	}

	return err
}

type Base struct {
	Value     interface{}
	Set       bool
	rules     []v.Rule
	preparers []apiHelpers.ParamFieldPreparer
}

func (p Base) Validate() error {
	err := v.ValidateStruct(&p,
		v.Field(&p.Value, p.rules...),
	)

	return extractError(err)
}

func (p *Base) Prepare() error {
	if p.preparers == nil || len(p.preparers) == 0 {
		return nil
	}

	if p.Value == nil {
		return nil
	}

	err := apiHelpers.PrepareParams(p.Value, nil, p.preparers...)

	return err
}

func (p *Base) SetRules(rules []v.Rule) {
	p.rules = rules
}

func (p *Base) SetPreparers(paramFieldPreparers []apiHelpers.ParamFieldPreparer) {
	p.preparers = paramFieldPreparers
}

type Int64 struct {
	Base
}

func (v *Int64) GetValue() *int64 {
	if v.Value == nil {
		return nil
	}

	return v.Value.(*int64)
}

func (v *Int64) UnmarshalJSON(data []byte) error {
	v.Set = true
	var temp int64
	v.Value = &temp
	return json.Unmarshal(data, &v.Value)
}

type String struct {
	Base
}

func (v *String) GetValue() *string {
	if v.Value == nil {
		return nil
	}

	return v.Value.(*string)
}

func (v *String) UnmarshalJSON(data []byte) error {
	v.Set = true
	var temp string
	v.Value = &temp
	return json.Unmarshal(data, &v.Value)
}

type Float64 struct {
	Base
}

func (v *Float64) GetValue() *float64 {
	if v.Value == nil {
		return nil
	}

	return v.Value.(*float64)
}

func (v *Float64) UnmarshalJSON(data []byte) error {
	v.Set = true
	var temp float64
	v.Value = &temp
	return json.Unmarshal(data, &v.Value)
}

type Bool struct {
	Base
}

func (v *Bool) GetValue() *bool {
	if v.Value == nil {
		return nil
	}

	return v.Value.(*bool)
}

func (v *Bool) UnmarshalJSON(data []byte) error {
	v.Set = true
	var temp bool
	v.Value = &temp
	return json.Unmarshal(data, &v.Value)
}

/*
time.Time representations
*/
type Time struct {
	Base
}

func (v *Time) GetValue() *time.Time {
	if v.Value == nil {
		return nil
	}

	return v.Value.(*time.Time)
}

func (v *Time) UnmarshalJSON(data []byte) error {
	v.Set = true
	var temp time.Time
	v.Value = &temp
	return json.Unmarshal(data, &v.Value)
}

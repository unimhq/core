package errors

type Public struct {
	NestedError error
	MessageID   string
}

func (public Public) Error() string {
	return public.NestedError.Error()
}

func NewPublic(nestedError error, messageID string) *Public {
	return &Public{
		NestedError: nestedError,
		MessageID:   messageID,
	}
}

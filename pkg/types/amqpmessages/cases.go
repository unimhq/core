package amqpmessages

import "github.com/satori/go.uuid"

type MccCaseStatusChange struct {
	CaseID uuid.UUID `json:"caseId"`
	Status string    `json:"status"`
}

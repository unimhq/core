package sortorders

import "bitbucket.org/unimhq/core/pkg/types"

const (
	ASC  types.SortOrder = "ASC"
	DESC types.SortOrder = "DESC"
)

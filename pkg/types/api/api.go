package api

import (
	"fmt"
	"io"
	"net/http"
	"net/url"
	"path/filepath"
	"regexp"
	"runtime"
)

type ErrorCode int64

const InternalServerErrorCode ErrorCode = 1
const MalformedParamsErrorCode ErrorCode = 2
const InvalidParamsErrorCode ErrorCode = 3
const AccessDeniedCode ErrorCode = 4
const BusinessLogicErrorCode ErrorCode = 5

const JwtAuthorizationErrorCode ErrorCode = 100
const JwtAuthHeaderWrongFormatErrorCode ErrorCode = 101
const JwtTokenNotFoundErrorCode ErrorCode = 102
const JwtTokenAlgValidationErrorCode ErrorCode = 103
const JwtTokenValidationErrorCode ErrorCode = 104
const JwtTokenMalformedErrorCode ErrorCode = 105
const JwtTokenUnverifiableErrorCode ErrorCode = 106
const JwtTokenInvalidSignatureErrorCode ErrorCode = 107
const JwtTokenExpiredErrorCode ErrorCode = 108
const JwtTokenNotIssuedErrorCode ErrorCode = 109
const JwtTokenNotValidYetErrorCode ErrorCode = 110

const IncorrectLoginOrPasswordErrorCode ErrorCode = 150
const ImpossibleLoginErrorCode ErrorCode = 151

type EmptySuccess map[string]string

var ErrorMessagesByCode = map[ErrorCode]string{
	InternalServerErrorCode:           "Internal server error",
	InvalidParamsErrorCode:            "Invalid params",
	AccessDeniedCode:                  "Access denied",
	BusinessLogicErrorCode:            "Business logic error",
	MalformedParamsErrorCode:          "Malformed params",
	JwtAuthorizationErrorCode:         "Token authorization error",
	JwtAuthHeaderWrongFormatErrorCode: "Authorization header format must be Bearer {token}",
	JwtTokenNotFoundErrorCode:         "Token was not found",
	JwtTokenAlgValidationErrorCode:    "Token validating algorithms mismatched",
	JwtTokenValidationErrorCode:       "Token is invalid",
	JwtTokenMalformedErrorCode:        "Token is malformed",
	JwtTokenUnverifiableErrorCode:     "Token is unverifiable",
	JwtTokenInvalidSignatureErrorCode: "Token signature is invalid",
	JwtTokenExpiredErrorCode:          "Token is expired",
	JwtTokenNotIssuedErrorCode:        "Token used before issued",
	JwtTokenNotValidYetErrorCode:      "Token is not valid yet",
	IncorrectLoginOrPasswordErrorCode: "Incorrect login or password",
	ImpossibleLoginErrorCode:          "Login is not possible",
}

func ErrorCodeToHTTPStatusCode(code ErrorCode) int {
	switch code {
	case MalformedParamsErrorCode:
		return http.StatusBadRequest
	case InvalidParamsErrorCode:
		return http.StatusBadRequest
	case AccessDeniedCode:
		return http.StatusForbidden
	case BusinessLogicErrorCode:
		return http.StatusBadRequest
	default:
		return http.StatusInternalServerError
	}
}

// Response contains Success flag, error (if occurred, nil otherwise) errorCode (if error occurred, 0 otherwise) Data (nil if no data)
type Response struct {
	Success                    bool        `json:"success"`
	Data                       interface{} `json:"data"`
	Error                      interface{} `json:"error"`
	ErrorCode                  ErrorCode   `json:"errorCode"`
	TranslateErrorToHttpStatus bool        `json:"-"`
}

func (r *Response) ToHttpStatus() *Response {
	r.TranslateErrorToHttpStatus = true

	return r
}

var funcNameRegexp = regexp.MustCompile(`^.*\.(.*)$`)

type MalformedApiParams struct {
	Err error
}

func (wrongApiParams *MalformedApiParams) Error() string {
	return wrongApiParams.Err.Error()
}

func NewWrongApiParams(err error) MalformedApiParams {
	return MalformedApiParams{
		Err: err,
	}
}

type ApiInternalError struct {
	Err                   error
	MethodParams          interface{}
	MethodName            string
	Message               string
	Context               map[string]interface{}
	TranslateToHttpStatus bool
}

func (apiInternalError *ApiInternalError) Error() string {
	return apiInternalError.Err.Error()
}

func (e *ApiInternalError) WithMethodParams(methodParams interface{}) *ApiInternalError {
	e.MethodParams = methodParams

	return e
}

func (e *ApiInternalError) WithMessage(message string) *ApiInternalError {
	e.Message = message

	return e
}

func (e *ApiInternalError) WithContext(context map[string]interface{}) *ApiInternalError {
	e.Context = context

	return e
}

func (e *ApiInternalError) WithTranslateToHttpStatus() *ApiInternalError {
	e.TranslateToHttpStatus = true

	return e
}

func NewApiIntErrBuilder(nestedError error) *ApiInternalError {
	pc, file, line, _ := runtime.Caller(1)
	funcObj := runtime.FuncForPC(pc)
	functionName := funcNameRegexp.ReplaceAllString(funcObj.Name(), "$1")

	dir, file := filepath.Split(file)
	base := filepath.Base(dir)

	methodName := fmt.Sprintf("%s.%v#%v", base, functionName, line)
	return &ApiInternalError{
		Err:        nestedError,
		MethodName: methodName,
	}
}

func NewApiIntErr(nestedError error, methodParams interface{}, message string, context map[string]interface{}) error {
	apiIntErrorBuilder := NewApiIntErrBuilder(nestedError)

	return apiIntErrorBuilder.
		WithMethodParams(methodParams).
		WithMessage(message).
		WithContext(context)
}

/**
Interface for all validatable things
*/
type Validatable interface {
	/**
	Validate params, before further use
	*/
	Validate() error
}

/**
Interface for all preparable things
*/
type Preparable interface {
	/**
	Performs preparing params before validation and using in future, e.g. sanitize html tags, strip spaces and etc.
	*/
	Prepare() error
}

/**
Interface for all API params in UNIM universe
*/
type Params interface {
	Validatable
	Preparable
}

/**
Interface for initialize something from Reader, e.g. initialize API params from request body
*/
type FromReaderInitializer interface {
	InitializeFromReader(io.Reader) error
}

/**
Interface for initialize something from URL, e.g. initialize API params from request URL params
*/
type FromURLInitializer interface {
	InitializeFromURL(url.URL) error
}

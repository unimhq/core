package metadata

import (
	"bitbucket.org/unimhq/core/pkg/localbus"
	"context"
	"database/sql/driver"
	"encoding/json"
	"reflect"
)

type Key string

type TableName string

type MetadataFieldName string

// type for store custom jsonb
type Metadata map[Key]interface{}

var Empty = make(Metadata)

func NotNil(m Metadata) Metadata {
	if m != nil {
		return m
	}

	return make(Metadata)
}

func (m Metadata) Merge(delta Metadata) Metadata {
	newMetadata := make(Metadata)
	for k, v := range m {
		newMetadata[k] = v
	}

	for k, v := range delta {
		newMetadata[k] = v
	}
	return newMetadata
}

func isNilOrEmpty(value interface{}) bool {
	reflectValue := reflect.ValueOf(value)

	if reflectValue.Kind() == reflect.Array {
		return false
	}

	if reflectValue.Kind() == reflect.String { // todo(egor): check *string
		strValue := value.(string)
		return len(strValue) == 0
	}

	if value == nil {
		return true
	}

	return reflect.ValueOf(value).IsNil()
}

func (m Metadata) TrimEmpty() Metadata {
	newMetadata := make(Metadata)
	for k, v := range m {
		if !isNilOrEmpty(v) {
			newMetadata[k] = v
		}
	}

	return newMetadata
}

func (m Metadata) With(key Key, value interface{}) Metadata {
	newMetadata := make(Metadata)
	for k, v := range m {
		newMetadata[k] = v
	}
	newMetadata[key] = value

	return newMetadata
}

/**
return new metadata without key, if it exists before
*/
func (m Metadata) Without(removeKey Key) Metadata {
	newMetadata := make(Metadata)
	for k, v := range m {
		if k != removeKey {
			newMetadata[k] = v
		}
	}

	return newMetadata
}

/*
 ignore nil values
*/
func (m Metadata) WithNotNil(key Key, value interface{}) Metadata {
	if isNilOrEmpty(value) {
		return m
	}

	return m.With(key, value)
}

func (m *Metadata) Scan(src interface{}) error {
	if data, ok := src.([]byte); ok {
		return json.Unmarshal(data, &m)
	}
	slogger().Warnf("Can't deserialize metadata, value: %v", src)

	return nil
}

func (m Metadata) Value() (driver.Value, error) {
	return json.Marshal(m)
}

type Service interface {
	RegisterUpdateHook(hook UpdateHook)

	Get(ctx context.Context, request GetRequest) (*GetResponse, error)

	/*
		Update metadata for entity with merge in DB (storage)
	*/
	Update(ctx context.Context, request UpdateRequest) (*UpdateResponse, error)
}

/*
Can be used for security and other purposes.
Can mutate original request - trim strings, etc
*/
type UpdateHook func(ctx context.Context, data UpdateHookData) (UpdateRequest, error)

type UpdateHookData struct {
	Request UpdateRequest
}

const MetadataUpdatedTopic localbus.Topic = "MetadataUpdated"

type MetadataUpdatedEvent struct {
	Table TableName

	Id string

	OldMetadata Metadata

	Delta Metadata
}

func (e MetadataUpdatedEvent) GetTopicName() localbus.Topic {
	return MetadataUpdatedTopic
}

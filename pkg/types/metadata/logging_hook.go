package metadata

import (
	"context"
)

func LoggingHook(ctx context.Context, data UpdateHookData) (UpdateRequest, error) {
	slogger().Infof("Update metadata for: %q / %q, delta: %+v",
		string(*data.Request.Table),
		*data.Request.Id,
		data.Request.Delta)

	return data.Request, nil
}

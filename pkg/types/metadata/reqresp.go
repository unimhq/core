package metadata

import "bitbucket.org/unimhq/core/pkg/empty"

type UpdateRequest struct {
	Table *TableName `json:"table"`

	Id *string `json:"id"`
	/*
		if metadata filed is nil - it will be removed from current metadata.
	*/
	Delta *Metadata `json:"delta"`
}

type UpdateResponse empty.EmptyStruct

type GetRequest struct {
	Table *TableName `json:"table"`

	Id *string `json:"id"`
}

type GetResponse struct {
	Metadata Metadata `json:"metadata"`
}

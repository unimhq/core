package metadata

import (
	"testing"
)

func TestNotNilOnNotEmptyString(t *testing.T) {
	if isNilOrEmpty("some") {
		t.FailNow()
	}
}

func TestNotNilOnEmptyString(t *testing.T) {
	if !isNilOrEmpty("") {
		t.FailNow()
	}
}

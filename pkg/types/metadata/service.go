package metadata

import (
	coreDbHelpers "bitbucket.org/unimhq/core/pkg/helpers/db"
	"bitbucket.org/unimhq/core/pkg/localbus"
	"bitbucket.org/unimhq/core/pkg/services/transactionmanager"
	"context"
	"fmt"
	sq "github.com/Masterminds/squirrel"
	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"go.uber.org/zap"
)

func slogger() *zap.SugaredLogger {
	return zap.S().Named("metadata")
}

type service struct {
	db                 *sqlx.DB
	transactionManager transactionmanager.TransactionManager
	hooks              []UpdateHook
	localBus           localbus.Bus
}

func (s *service) RegisterUpdateHook(hook UpdateHook) {
	s.hooks = append(s.hooks, hook)
}

func (s *service) Get(ctx context.Context, request GetRequest) (*GetResponse, error) {
	if vr := request.Validate(ctx); !vr.IsValid() {
		return nil, errors.Wrap(vr.ToError(), "Can't perform `Get` with invalid arguments")
	}

	metadata, err := s.getMetadataOfEntity(ctx, *request.Table, *request.Id)
	if err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("Can't get metadata of entity. Request: %+v", request))
	}

	return &GetResponse{
		Metadata: *metadata,
	}, nil
}

func (s *service) Update(ctx context.Context, request UpdateRequest) (*UpdateResponse, error) {
	slogger().Infof("update metadata: %+v", request)

	if vr := request.Validate(ctx); !vr.IsValid() {
		return nil, errors.Wrap(vr.ToError(), "Can't perform `Update` with invalid arguments")
	}

	err := s.transactionManager.RunInTransaction(ctx, func(ctx context.Context) error {
		r := request
		for _, hook := range s.hooks {
			hookData := UpdateHookData{
				Request: r,
			}
			newRequest, err := hook(ctx, hookData)
			if err != nil {
				return errors.Wrap(err, fmt.Sprintf("Hook failed %v on update metadata, %+v", hook, hookData))
			}

			r = newRequest
		}

		err := s.updateMetadataOfEntity(ctx, *request.Table, *request.Id, *request.Delta)
		if err != nil {
			return errors.Wrap(err, "Can't save metadata in db")
		}

		err = s.localBus.Publish(ctx, MetadataUpdatedEvent{
			Table: *request.Table,
			Id:    *request.Id,
			Delta: *request.Delta,
		})
		if err != nil {
			return errors.Wrap(err, "Can't publish event to local bus")
		}

		return nil
	})

	if err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("Can't perform operation, request = %+v", request))
	}

	return &UpdateResponse{}, nil
}

func (s *service) getMetadataOfEntity(ctx context.Context, table TableName, id string) (*Metadata, error) {
	db := coreDbHelpers.GetFromContextOrUseExisting(ctx, s.db)

	q, args, err := sq.Select("metadata").
		From(string(table)).
		Where(sq.Eq{"id": id}).
		PlaceholderFormat(sq.Dollar).
		ToSql()

	if err != nil {
		return nil, errors.Wrap(err, "getMetadataOfEntity, unable to build sql query")
	}

	entity := entityWithMetadata{}

	row := db.QueryRowx(q, args...)

	err = row.StructScan(&entity)

	if err != nil {
		return nil, coreDbHelpers.EmptyOrError(err, "getMetadataOfEntity, unable to get case by query")
	}

	return &entity.Metadata, nil
}

func (s *service) updateMetadataOfEntity(ctx context.Context, table TableName, id string, metadata Metadata) error {
	db := coreDbHelpers.GetFromContextOrUseExisting(ctx, s.db)

	q, args, err := sq.Update(string(table)).
		Set("metadata", sq.Expr("coalesce(metadata, '{}'::jsonb) || ?", metadata)).
		Where(sq.Eq{"id": id}).
		PlaceholderFormat(sq.Dollar).
		ToSql()

	if err != nil {
		return errors.Wrap(err, "updateMetadataOfEntity, unable to build query")
	}

	_, err = db.Exec(q, args...)

	if err != nil {
		return errors.Wrap(err, "Db update failed")
	}

	return nil
}

type entityWithMetadata struct {
	Metadata Metadata `db:"metadata"`
}

func NewService(
	db *sqlx.DB,
	strictMapping bool,
	transactionManager transactionmanager.TransactionManager,
	localBus localbus.Bus,
) (Service, error) {
	if db == nil {
		return nil, errors.New("NewService, db must be not empty")
	}

	if transactionManager == nil {
		return nil, errors.New("NewService, transactionManager must be not empty")
	}

	if localBus == nil {
		return nil, errors.New("NewService, localBus must be not empty")
	}

	localBus.DisableWarningOnEmptySubscribers(MetadataUpdatedTopic)

	resultDB := db
	if !strictMapping {
		resultDB = db.Unsafe()
	}

	srv := service{
		db:                 resultDB,
		transactionManager: transactionManager,
		localBus:           localBus,
	}

	srv.RegisterUpdateHook(LoggingHook)

	return &srv, nil
}

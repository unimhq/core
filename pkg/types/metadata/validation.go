package metadata

import (
	"bitbucket.org/unimhq/core/pkg/validation"
	"context"
)

func (r UpdateRequest) Validate(ctx context.Context) validation.ValidationResult {
	builder := validation.Ok()

	if r.Table == nil {
		builder = builder.Add("table", validation.NotNilError)
	}

	if r.Id == nil {
		builder = builder.Add("id", validation.NotNilError)
	}

	if r.Delta == nil {
		builder = builder.Add("delta", validation.NotNilError)
	}

	return builder.Build()
}

func (r GetRequest) Validate(ctx context.Context) validation.ValidationResult {
	builder := validation.Ok()

	if r.Table == nil {
		builder = builder.Add("table", validation.NotNilError)
	}

	if r.Id == nil {
		builder = builder.Add("id", validation.NotNilError)
	}

	return builder.Build()
}

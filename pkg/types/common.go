package types

type App string

const DP App = "DP"
const LIS App = "LIS"

type SortOrder string

type AmqpRoutingKey string
type AmqpExchangeName string

type AsyncResult struct {
	Value interface{}
	Error error
}

type Localizable interface {
	GetLocalizationKey() string
}

//MapSS is just alias for map string => string
type MapSS map[string]string

//MapSI is just alias for map string => interface{}
type MapSI map[string]interface{}

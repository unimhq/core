package transactionmanager

import (
	"context"
)

type RunWithTransaction func(ctx context.Context) error

// todo(egor): move to core
type TransactionManager interface {
	// Transaction(db connection) is stored in context
	// If transaction already started caller must handle errors and rollback if required
	// If no external transaction exists - start new transaction and commit on success, rollback - on error or panic after block execution
	RunInTransaction(ctx context.Context, block RunWithTransaction) error
}

package transactionmanager

import (
	coreDbHelpers "bitbucket.org/unimhq/core/pkg/helpers/db"
	"context"
	"fmt"
	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"go.uber.org/zap"
)

type service struct {
	db sqlx.Ext
}

func (s service) RunInTransaction(ctx context.Context, block RunWithTransaction) error {
	db := coreDbHelpers.GetFromContextOrUseExisting(ctx, s.db)

	createdTransaction, err := extractTx(db)

	defer func() { // rollback on panic
		if err := recover(); err != nil {
			if createdTransaction.isNew { // rollback only new transaction, for old - delegate tx management to caller
				zap.S().Warnw("Rollback transaction, reason is panic", "panicError", err)

				rollbackError := createdTransaction.tx.Rollback()
				if rollbackError != nil {
					panic(errors.Wrap(rollbackError, fmt.Sprintf("Can't rollback transaction after error %s", err)))
				}
			}

			panic(err)
		}
	}()

	if err != nil {
		return errors.Wrap(err, "Can't extract transaction")
	}

	contextWithTransaction := coreDbHelpers.PutTransactionToContext(ctx, *createdTransaction.tx)

	blockError := block(contextWithTransaction)

	if blockError != nil {
		if createdTransaction.isNew { // rollback only new transaction, for old - delegate tx management to caller
			zap.S().Warnw("Rollback transaction, reason is error: ", "blockError", blockError)
			rollbackError := createdTransaction.tx.Rollback()
			if rollbackError != nil {
				return errors.Wrap(rollbackError, fmt.Sprintf("Can't rollback transactiona after error %s", blockError))
			}
		}

		return errors.Wrap(blockError, "Block run failed")
	}

	if createdTransaction.isNew {
		commitError := createdTransaction.tx.Commit()
		if commitError != nil {
			return errors.Wrap(commitError, "Can't commit transaction after block run")
		}
	}

	return nil
}

type createdTransaction struct {
	isNew bool
	tx    *sqlx.Tx
}

func extractTx(ext sqlx.Ext) (*createdTransaction, error) {
	currentTx, ok := ext.(*sqlx.Tx)
	if ok {
		return &createdTransaction{false, currentTx}, nil
	}

	db, ok := ext.(*sqlx.DB)

	if !ok {
		return nil, errors.New("Unsupported type of sqx.Ext")
	}

	newTx := db.MustBegin()

	return &createdTransaction{true, newTx}, nil
}

func New(db *sqlx.DB, strictMapping bool) (*service, error) {
	if db == nil {
		return nil, errors.New("NewRepository, db must be not empty")
	}

	resultDB := db
	if !strictMapping {
		resultDB = db.Unsafe()
	}

	return &service{
		db: resultDB,
	}, nil
}

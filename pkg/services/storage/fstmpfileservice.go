package storage

import (
	"bitbucket.org/unimhq/core/pkg/helpers/timeformat"
	"encoding/json"
	"fmt"
	"github.com/pkg/errors"
	"github.com/satori/go.uuid"
	"io"
	"os"
	"time"
)

const defaultFilePrefix = "tmp-file"
const tempStoreKeyPrefix = "tmp-files"
const tempStoreKeyTTL = 60 * 60 * 24 // 24 hours

type fileInfo struct {
	Name      string        `json:"name"`
	CreatedAt string        `json:"createdAt"`
	Metadata  *FileMetaData `json:"metadata"`
}

type fsTempStoreFileService struct {
	tempStoreService TempStoreService
	rootDir          string
	filePrefix       string
}

func (s *fsTempStoreFileService) Save(reader io.Reader, metaData *FileMetaData, ttl int64) (string, error) {
	fileID := s.buildFileID()
	fileName := s.buildFileName(fileID)
	filePath := s.buildFullFilePath(fileName)

	file, err := os.Create(filePath)

	if err != nil {
		return "", errors.Wrap(err, "fsTempStoreFileService.Save, unable to create file")
	}

	defer file.Close()

	_, err = io.Copy(file, reader)

	if err != nil {
		return "", errors.Wrap(err, "fsTempStoreFileService.Save, unable to copy file")
	}

	fi := fileInfo{
		Name:      fileName,
		Metadata:  metaData,
		CreatedAt: time.Now().Format(timeformat.Datetime),
	}

	fileInfoBytes, err := json.Marshal(fi)

	if err != nil {
		return "", errors.Wrap(err, "fsTempStoreFileService.Save, unable to convert fileInfo to JSON")
	}

	resultTTL := ttl

	if ttl < 0 {
		resultTTL = tempStoreKeyTTL
	}

	err = s.tempStoreService.Set(s.buildTempStoreKey(fileID), string(fileInfoBytes), resultTTL)

	if err != nil {
		return "", errors.Wrap(err, "fsTempStoreFileService.Save, unable to save fileInfo to temp store")
	}

	return fileID, nil
}

func (s *fsTempStoreFileService) Get(ID string) (io.ReadCloser, *FileMetaData, error) {
	fi, err := s.getFileInfo(ID)

	if err != nil {
		return nil, nil, errors.Wrap(err, "fsTempStoreFileService.Get, unable to get file info")
	}

	if fi == nil {
		return nil, nil, errors.Wrap(err, "fsTempStoreFileService.Get, file not exists")
	}

	filePath := s.buildFullFilePath(fi.Name)

	if _, err := os.Stat(filePath); os.IsNotExist(err) {
		return nil, nil, errors.Wrap(err, fmt.Sprintf("fsTempStoreFileService.Get, file not exists, path: %s", filePath))
	}

	file, err := os.Open(filePath)

	if err != nil {
		return nil, nil, errors.Wrap(err, "fsTempStoreFileService.Get, unable to open file")
	}

	return file, fi.Metadata, nil
}

func (s *fsTempStoreFileService) Delete(ID string) error {
	fi, err := s.getFileInfo(ID)

	if err != nil {
		return errors.Wrap(err, "fsTempStoreFileService.Delete, unable to get file info")
	}

	if fi == nil {
		return nil
	}

	filePath := s.buildFullFilePath(fi.Name)

	if _, err := os.Stat(filePath); os.IsNotExist(err) {
		return nil
	}

	err = os.Remove(filePath)

	if err != nil {
		return errors.Wrap(err, "fsTempStoreFileService.Delete, unable to delete file")
	}

	s.tempStoreService.Del(s.buildTempStoreKey(ID))

	return nil
}

func (s *fsTempStoreFileService) HasFile(ID string) (bool, error) {
	fileInfo, err := s.getFileInfo(ID)

	if err != nil {
		return false, errors.Wrap(err, "fsTempStoreFileService.HasFile, unable to file info")
	}

	return fileInfo != nil, nil
}

func (s *fsTempStoreFileService) GetFileMetadata(ID string) (*FileMetaData, error) {
	fileInfo, err := s.getFileInfo(ID)

	if err != nil {
		return nil, errors.Wrap(err, "fsTempStoreFileService.GetFileMetadata, unable to file info")
	}

	if fileInfo == nil {
		return nil, errors.New(fmt.Sprintf("fsTempStoreFileService.GetFileMetadata, file info was not found by id: %s", ID))
	}

	return fileInfo.Metadata, nil
}

func (s *fsTempStoreFileService) getFileInfo(ID string) (*fileInfo, error) {
	key := s.buildTempStoreKey(ID)

	raw, err := s.tempStoreService.Get(key)

	fi := fileInfo{}

	if err != nil {
		return nil, errors.Wrap(err, "fsTempStoreFileService.getFileInfo, unable to get fileInfo from temp store")
	}

	if raw == nil {
		return nil, nil
	}

	err = json.Unmarshal([]byte(*raw), &fi)

	if err != nil {
		return nil, errors.Wrap(err, "fsTempStoreFileService.getFileInfo, unable to decode file info from JSON")
	}

	return &fi, nil
}

func (s *fsTempStoreFileService) buildFileID() string {
	return uuid.NewV4().String()
}

func (s *fsTempStoreFileService) buildFullFilePath(fileName string) string {
	return fmt.Sprintf("%s/%s", s.rootDir, fileName)
}

func (s *fsTempStoreFileService) buildFileName(ID string) string {
	return fmt.Sprintf("%s-%s", s.filePrefix, ID)
}

func (s *fsTempStoreFileService) buildTempStoreKey(ID string) string {
	return fmt.Sprintf("%s-%s", tempStoreKeyPrefix, ID)
}

func NewFsTempStoreFileService(tempStoreService TempStoreService, rootDir string, filePrefix *string) (*fsTempStoreFileService, error) {
	if tempStoreService == nil {
		return nil, errors.New("tempStoreService must be not empty")
	}

	if _, err := os.Stat(rootDir); os.IsNotExist(err) {
		return nil, errors.Wrap(err, "NewFsTempStoreFileService, root directory is not exists")
	}

	resultFilePrefix := defaultFilePrefix

	if filePrefix != nil {
		resultFilePrefix = *filePrefix
	}

	return &fsTempStoreFileService{
		tempStoreService: tempStoreService,
		rootDir:          rootDir,
		filePrefix:       resultFilePrefix,
	}, nil
}

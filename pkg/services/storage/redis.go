package storage

import (
	"github.com/garyburd/redigo/redis"
	"github.com/pkg/errors"
)

type redisTempStoreService struct {
	redisPool *redis.Pool
}

func (tempStoreService *redisTempStoreService) Get(key string) (*string, error) {
	conn := tempStoreService.redisPool.Get()
	defer conn.Close()

	result, err := redis.String(conn.Do("GET", key))

	if err == redis.ErrNil {
		return nil, nil
	}

	if err != nil {
		return nil, errors.Wrap(err, "redisTempStoreService.Get, unable to execute GET command")
	}

	return &result, nil
}

func (tempStoreService *redisTempStoreService) Set(key string, value string, ttl int64) error {
	conn := tempStoreService.redisPool.Get()
	defer conn.Close()

	var err error
	if ttl > 0 {
		_, err = conn.Do("SETEX", key, ttl, value)
	} else {
		_, err = conn.Do("SET", key, value)
	}

	if err != nil {
		return errors.Wrap(err, "redisTempStoreService.Set, unable to execute SET/SETEX command")
	}

	return nil
}

func (tempStoreService *redisTempStoreService) Del(key string) error {
	conn := tempStoreService.redisPool.Get()
	defer conn.Close()

	_, err := conn.Do("DEL", key)

	if err != nil {
		return errors.Wrap(err, "redisTempStoreService.Del, unable to execute DEL command")
	}

	return nil
}

func NewRedisTempStoreService(redisPool *redis.Pool) (*redisTempStoreService, error) {
	if redisPool == nil {
		return nil, errors.New("redis pool must be not empty")
	}

	return &redisTempStoreService{
		redisPool: redisPool,
	}, nil
}

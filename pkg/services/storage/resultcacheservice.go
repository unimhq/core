package storage

import (
	"encoding/json"
	"github.com/pkg/errors"
	"go.uber.org/zap"
	"reflect"
)

type resultCacheService struct {
	logger           *zap.Logger
	tempStoreService TempStoreService
	defaultTTL       int64
}

func (s *resultCacheService) Get(key string, resultType reflect.Type, fetchFunc ResultFetchFunc, options *CacheOptions) (interface{}, error) {
	cachedStat, err := s.tempStoreService.Get(key)

	if err != nil {
		s.logger.Warn("resultCacheService.Get, unable to get stat from temp storage", zap.String("key", key))
	}

	result := reflect.New(resultType).Interface()

	if cachedStat != nil {
		err := json.Unmarshal([]byte(*cachedStat), result)

		if err == nil {
			return result, nil
		}

		s.logger.Warn("resultCacheService.Get, unable to decode results from temp storage", zap.String("cachedValue", *cachedStat))
	}

	result, err = fetchFunc()

	if err != nil {
		return nil, errors.Wrap(err, "resultCacheService.Get, unable to get result from fetch function service")
	}

	resultJson, err := json.Marshal(result)

	if err != nil {
		return nil, errors.Wrap(err, "resultCacheService.Get, unable to serialize result to JSON")
	}

	resultTTL := s.defaultTTL

	if options != nil && options.TTL != 0 {
		resultTTL = options.TTL
	}

	err = s.tempStoreService.Set(key, string(resultJson), resultTTL)

	if err != nil {
		return nil, errors.Wrap(err, "resultCacheService.Get, unable to store result to temp storage")
	}

	return result, nil
}

func NewResultCacheService(logger *zap.Logger, tempStoreService TempStoreService, defaultTTL int64) (*resultCacheService, error) {
	if logger == nil {
		return nil, errors.New("logger must be not empty")
	}

	if tempStoreService == nil {
		return nil, errors.New("tempStoreService must be not empty")
	}

	return &resultCacheService{
		logger:           logger,
		tempStoreService: tempStoreService,
		defaultTTL:       defaultTTL,
	}, nil
}

package storage

import (
	"bitbucket.org/unimhq/core/pkg/types"
	"io"
	"reflect"
)

type FileMetaData struct {
	OriginalName string      `json:"originalName"`
	Size         int64       `json:"size"`
	MimeType     string      `json:"mimeType"`
	Extra        types.MapSI `json:"extra"`
}

type TempStoreService interface {
	Get(key string) (*string, error)
	Set(key string, value string, ttl int64) error
	Del(key string) error
}

type ResultFetchFunc func() (interface{}, error)

type CacheOptions struct {
	TTL int64
}

type ResultCacheService interface {
	Get(key string, resultType reflect.Type, fetchFunc ResultFetchFunc, options *CacheOptions) (interface{}, error)
}

type TempStoreFileService interface {
	Save(reader io.Reader, metaData *FileMetaData, ttl int64) (string, error)
	Get(ID string) (io.ReadCloser, *FileMetaData, error)
	GetFileMetadata(ID string) (*FileMetaData, error)
	HasFile(ID string) (bool, error)
	Delete(ID string) error
}

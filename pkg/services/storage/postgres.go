package storage

import (
	dbHelpers "bitbucket.org/unimhq/core/pkg/helpers/db"
	sq "github.com/Masterminds/squirrel"
	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"time"
)

type kvStorageItem struct {
	ID        int64      `db:"id"`
	CreatedAt time.Time  `db:"created_at"`
	UpdatedAt time.Time  `db:"updated_at"`
	DeletedAt *time.Time `db:"deleted_at"`
	Key       string     `db:"key"`
	Value     *string    `db:"value"`
}

type postgresTempStoreService struct {
	db                 sqlx.Ext
	kvStorageTableName string
}

func (s *postgresTempStoreService) Get(key string) (*string, error) {
	q, args, err := sq.Select("*").
		From(s.kvStorageTableName).
		Where(sq.Eq{"key": key}).
		Where(sq.Or{
			sq.Gt{"deleted_at": time.Now()},
			sq.Eq{"deleted_at": nil}}).
		PlaceholderFormat(sq.Dollar).
		ToSql()

	if err != nil {
		return nil, errors.Wrap(err, "postgresTempStoreService.Get, unable to build sql query")
	}

	item := kvStorageItem{}

	row := s.db.QueryRowx(q, args...)
	err = row.StructScan(&item)

	if err != nil {
		return nil, dbHelpers.EmptyOrError(err, "postgresTempStoreService.Get, unable to get value by key")
	}

	return item.Value, nil
}

func (s *postgresTempStoreService) Set(key string, value string, ttl int64) error {
	var deletedAt *time.Time = nil

	now := time.Now()

	if ttl > 0 {
		tmpDeletedAt := now.Add(time.Duration(ttl) * time.Second)
		deletedAt = &tmpDeletedAt
	}

	item := kvStorageItem{
		CreatedAt: now,
		UpdatedAt: now,
		DeletedAt: deletedAt,
		Key:       key,
		Value:     &value,
	}

	q, args, err := sq.Insert(s.kvStorageTableName).
		Columns("created_at", "updated_at", "deleted_at", "key", "value").
		Values(item.CreatedAt, item.UpdatedAt, item.DeletedAt, item.Key, item.Value).
		Suffix("ON CONFLICT (key) DO UPDATE SET value = ?, updated_at = ?, deleted_at = ?", value, item.UpdatedAt, deletedAt).
		PlaceholderFormat(sq.Dollar).
		ToSql()

	if err != nil {
		return errors.Wrap(err, "postgresTempStoreService.Set, unable to build query")
	}

	_, err = s.db.Exec(q, args...)

	if err != nil {
		return errors.Wrap(err, "postgresTempStoreService.Set, unable to set value to temp storage table by key")
	}

	return nil
}

func (s *postgresTempStoreService) Del(key string) error {
	q, args, err := sq.Delete(s.kvStorageTableName).
		Where(sq.Eq{"key": key}).
		PlaceholderFormat(sq.Dollar).
		ToSql()

	if err != nil {
		return errors.Wrap(err, "postgresTempStoreService.Del, unable to build query")
	}

	_, err = s.db.Exec(q, args...)

	if err != nil {
		return errors.Wrap(err, "postgresTempStoreService.Del, unable to delete value by key")
	}

	return nil
}

func NewPostgresTempStoreService(db *sqlx.DB, kvStorageTableName string) (*postgresTempStoreService, error) {
	if db == nil {
		return nil, errors.New("NewRepository, db must be not empty")
	}

	return &postgresTempStoreService{
		db:                 db,
		kvStorageTableName: kvStorageTableName,
	}, nil
}

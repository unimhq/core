package redis

import (
	"github.com/garyburd/redigo/redis"
	"github.com/pkg/errors"
)

type storeService struct {
	redisPool *redis.Pool
}

func (redisStoreService *storeService) GetList(key string) ([]string, error) {
	conn := redisStoreService.redisPool.Get()
	defer conn.Close()

	llen, err := redis.Int64(conn.Do("LLEN", key))

	if err == redis.ErrNil {
		return nil, nil
	}

	if err != nil {
		return nil, errors.Wrap(err, "redisStoreService.GetList, unable to execute LLEN command")
	}

	result, err := redis.Strings(conn.Do("LRANGE", key, 0, llen))

	if err == redis.ErrNil {
		return nil, nil
	}

	if err != nil {
		return nil, errors.Wrap(err, "redisStoreService.GetList, unable to execute LRANGE command")
	}

	return result, nil
}

func (redisStoreService *storeService) Insert(key string, value string) error {
	conn := redisStoreService.redisPool.Get()
	defer conn.Close()

	_, err := redis.Int64(conn.Do("RPUSH", key, value))

	if err == redis.ErrNil {
		return nil
	}

	if err != nil {
		return errors.Wrap(err, "redisStoreService.Insert, unable to execute LPUSH command")
	}

	return nil
}

func (redisStoreService *storeService) IsExist(key string) (bool, error) {
	conn := redisStoreService.redisPool.Get()
	defer conn.Close()

	result, err := redis.Int64(conn.Do("EXISTS", key))

	if err == redis.ErrNil {
		return false, nil
	}

	if err != nil {
		return false, errors.Wrap(err, "redisStoreService.IsExist, unable to execute EXISTS command")
	}

	if result == 1 {
		return true, nil
	}

	return false, nil
}

func (redisStoreService *storeService) Del(key string) error {
	conn := redisStoreService.redisPool.Get()
	defer conn.Close()

	_, err := conn.Do("DEL", key)

	if err != nil {
		return errors.Wrap(err, "redisStoreService.Del, unable to execute DEL command")
	}

	return nil
}

func (redisStoreService *storeService) DelItemList(key string, count int, value string) error {
	conn := redisStoreService.redisPool.Get()
	defer conn.Close()

	_, err := conn.Do("LREM", key, count, value)

	if err != nil {
		return errors.Wrap(err, "redisStoreService.DelItemList, unable to execute LREM command")
	}

	return nil
}

func NewStoreService(redisPool *redis.Pool) (*storeService, error) {
	if redisPool == nil {
		return nil, errors.New("redis pool must be not empty")
	}

	return &storeService{
		redisPool: redisPool,
	}, nil
}

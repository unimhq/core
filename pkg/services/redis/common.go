package redis

type StoreService interface {
	GetList(key string) ([]string, error)
	Insert(key string, value string) error
	Del(key string) error
	DelItemList(key string, count int, value string) error
	IsExist(key string) (bool, error)
}

package services

import (
	"bitbucket.org/unimhq/core/pkg/helpers/cmd"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/pkg/errors"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/spf13/viper"
	"io/ioutil"
	"net/http"
	"os"
)

func StartWebServer(
	app App,
	r *mux.Router,
	port *int, // for backward compatibility
) {
	httpPort := getHttpServerPort(port, app.Config())

	registerManagementEndpoints(app, r, httpPort)

	fmt.Println(fmt.Sprintf("Start application %q on port %d", app.Name(), httpPort))

	runError := http.ListenAndServe(fmt.Sprintf(":%d", httpPort), r)

	cmd.DieIfError(runError, "Http server start failed")
}

func registerManagementEndpoints(
	app App,
	r *mux.Router,
	httpPort int,
) {
	r.Handle("/management/info", handleInfo(app, httpPort)).Methods("GET")
	r.Handle("/management/config", handleConfig(app)).Methods("GET")
	r.Handle("/management/metrics", promhttp.Handler()).Methods("GET")
}

func getHttpServerPort(port *int, config *viper.Viper) int {
	httpPort := 8080 // default http port

	if port != nil {
		httpPort = *port
	}

	portFromConfig := config.GetInt("web.port")
	if portFromConfig != 0 {
		httpPort = portFromConfig
	}

	return httpPort
}

// basic info about application
func handleInfo(app App, httpPort int) http.Handler {
	result := make(map[string]interface{})
	result["name"] = app.Name()
	result["httPort"] = httpPort
	result["deployInfo"] = app.DeployInfo()
	result["version"] = ApplicationVersion
	result["gitCommitId"] = GitCommitId
	result["gitCommitLink"] = GitCommitLink

	return http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		rw.Header().Add("Content-Type", "application/json")
		enc := json.NewEncoder(rw)
		enc.SetEscapeHTML(false)
		_ = enc.Encode(result)
	})
}

func handleConfig(app App) http.Handler {
	tempConfigFile, err := ioutil.TempFile("", fmt.Sprintf("confing-%s-*.json", app.Name()))
	if err != nil {
		panic(errors.Wrap(err, "Can't create temp file"))
	}

	err = app.Config().WriteConfigAs(tempConfigFile.Name())
	if err != nil {
		panic(errors.Wrap(err, "Can't write config to temp file"))
	}

	configAsYml, err := ioutil.ReadFile(tempConfigFile.Name())
	err = os.Remove(tempConfigFile.Name())
	if err != nil {
		panic(errors.Wrap(err, "Can't remove temp config file"))
	}

	return http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		rw.Header().Add("Content-Type", "application/json")
		rw.Write(configAsYml)
	})
}

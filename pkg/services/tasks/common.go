package tasks

import (
	"github.com/RichardKnop/machinery/v1"
	"github.com/RichardKnop/machinery/v1/tasks"
	"golang.org/x/tools/container/intsets"
)

type TaskName string

const (
	InfinityRetries = intsets.MaxInt
)

//BasTaskHandler ...
type BaseTaskHandler struct {
	Name   TaskName
	Handle interface{}
}

func (baseTaskHandler *BaseTaskHandler) GetName() TaskName {
	return baseTaskHandler.Name
}

func (baseTaskHandler *BaseTaskHandler) GetHandleFunc() interface{} {
	if baseTaskHandler.Handle == nil {
		panic("baseTaskHandler.GetHandleFunc, Handle function is nil")
	}

	return baseTaskHandler.Handle
}

//TaskHandlerParams ...
type TaskHandlerParams interface {
	GetTaskData() (*tasks.Signature, error)
}

type TaskHandler interface {
	GetName() TaskName
	GetHandleFunc() interface{}
}

type Manager interface {
	GetServer() *machinery.Server
	SendTask(taskParams TaskHandlerParams) error
	RegisterTask(task TaskHandler)
}

package tasks

import (
	"time"

	"bitbucket.org/unimhq/core/pkg/helpers/amqp"
	"github.com/RichardKnop/machinery/v1"
	"github.com/RichardKnop/machinery/v1/config"
	"github.com/pkg/errors"
)

type manager struct {
	brokerURL string
	resultURL string
	server    *machinery.Server
}

func (m *manager) GetServer() *machinery.Server {
	return m.server
}

func (m *manager) SendTask(taskParams TaskHandlerParams) error {
	taskData, err := taskParams.GetTaskData()

	if err != nil {
		return errors.Wrap(err, "manager.SendTask, unable to get task data")
	}

	_, err = m.server.SendTask(taskData)

	if err != nil {
		return errors.Wrap(err, "manager.SendTask, unable to send task")
	}

	return nil
}

func (m *manager) RegisterTask(task TaskHandler) {
	m.server.RegisterTask(string(task.GetName()), task.GetHandleFunc())
}

func NewTaskManager(brokerURL string,
	resultURL string,
	exchangeName string,
	defaultQueue string,
	bindingKey string,
	uniq string) (Manager, error) {
	const resultExpireInHours = time.Hour * 24

	if len(brokerURL) == 0 {
		return nil, errors.New("NewTaskManager, brokerURL must be not empty")
	}

	if len(resultURL) == 0 {
		return nil, errors.New("NewTaskManager, resultURL must be not empty")
	}

	if len(bindingKey) == 0 {
		return nil, errors.New("NewTaskManager, bindingKey must be not empty")
	}

	if len(defaultQueue) == 0 {
		return nil, errors.New("NewTaskManager, defaultQueue must be not empty")
	}

	cfg := config.Config{
		Broker:        brokerURL,
		DefaultQueue:  amqp.BuildUniqName(defaultQueue, uniq),
		ResultBackend: resultURL,
		AMQP: &config.AMQPConfig{
			Exchange:     amqp.BuildUniqName(exchangeName, uniq),
			ExchangeType: "direct",
			BindingKey:   amqp.BuildUniqName(bindingKey, uniq),
		},
		ResultsExpireIn: int((resultExpireInHours).Seconds()),
	}

	server, err := machinery.NewServer(&cfg)

	if err != nil {
		return nil, errors.Wrap(err, "tasks.NewTaskManager, unable to create new task server")
	}

	return &manager{server: server}, nil
}

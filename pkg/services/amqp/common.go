package amqp

import "github.com/streadway/amqp"

type ConnectionProvider interface {
	GetConnection() (*amqp.Connection, error)
	ProvideConnectionToCb(cb func(connection *amqp.Connection) error, cbName string, maxRetriesCount int64) error
}

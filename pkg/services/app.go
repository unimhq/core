package services

import (
	"fmt"
	"github.com/prometheus/client_golang/prometheus"
	"io"
	"time"

	"bitbucket.org/unimhq/core/pkg/config"
	amqpHelpers "bitbucket.org/unimhq/core/pkg/helpers/amqp"
	"bitbucket.org/unimhq/core/pkg/localbus"
	"bitbucket.org/unimhq/core/pkg/third_party/zapsentry"
	"github.com/garyburd/redigo/redis"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq" //enable postgres
	"github.com/pkg/errors"
	"github.com/spf13/viper"
	"github.com/streadway/amqp"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var ApplicationVersion = "UNKNOWN" // override with `go build -ldflags "-X bitbucket.org/unimhq/core/pkg/services.ApplicationVersion=x.y.z"`
var GitCommitId = "UNKNOWN"        // override with build
var GitCommitLink = "UNKNOWN"      // override with build

type App interface {
	Config() *viper.Viper
	Init() error
	Logger() *zap.Logger
	DBConnection() *sqlx.DB
	RedisPool() *redis.Pool
	BuildServiceLogger(service string) (*zap.Logger, error)
	BuildCustomDBConnection(host string, user string, pass string, port int, dbName string) (*sqlx.DB, error)
	// used in service discovery, dynamic config loading and as kafka consumer group
	Name() string
	/*
		for in-app communication
	*/
	LocalBus() localbus.Bus

	DeployInfo() config.DeployInfo
}

type app struct {
	initialized    bool
	config         *viper.Viper
	dbConnection   *sqlx.DB
	logger         *zap.Logger
	redisPool      *redis.Pool
	amqpConnection *amqp.Connection
	name           string
	bus            localbus.Bus

	defaultConfig io.Reader

	deployInfo config.DeployInfo
}

func (app *app) Name() string {
	return app.name
}

func (app *app) IsInitialized() bool {
	return app.initialized
}

func (app *app) Config() *viper.Viper {
	return app.config
}

func (app *app) Logger() *zap.Logger {
	return app.logger
}

func (app *app) DBConnection() *sqlx.DB {
	return app.dbConnection
}

func (app *app) RedisPool() *redis.Pool {
	return app.redisPool
}

func (app *app) LocalBus() localbus.Bus {
	return app.bus
}

func (app *app) DeployInfo() config.DeployInfo {
	return app.deployInfo
}

func (app *app) Init() error {
	cfg, deployInfo, err := config.ReadConfig(app.name, app.defaultConfig)
	if err != nil {
		return errors.Wrap(err, "app.Init, unable to read config")
	}
	app.config = cfg
	app.deployInfo = *deployInfo

	// todo(egor): override `application.name` from config. Useful for multiple deploy app in one env (prod, test..)

	logger, err := app.initLogger()
	if err != nil {
		return errors.Wrap(err, "app.Init, unable to init logger")
	}
	app.logger = logger

	err = app.initMetrics()
	if err != nil {
		return errors.Wrap(err, "can't init metrics")
	}

	// todo(egor): dbConnetion and redis pool must be exposed as separate module
	dbConnection, err := app.initDBConnection()
	if err != nil {
		return errors.Wrap(err, "app.Init, unable to init db connection")
	}
	app.dbConnection = dbConnection

	redisPool, err := app.initRedisPool()
	if err != nil {
		return errors.Wrap(err, "app.Init, unable to init redis pool")
	}
	app.redisPool = redisPool

	return nil
}

func (app *app) BuildCustomDBConnection(host string, user string, pass string, port int, dbName string) (*sqlx.DB, error) {
	var defaultPort = 5432

	if port == 0 {
		port = defaultPort
	}

	if len(host) == 0 {
		return nil, errors.New("app.BuildCustomDBConnection, host must be not empty")
	}

	if len(user) == 0 {
		return nil, errors.New("app.BuildCustomDBConnection, user must be not empty")
	}

	if len(pass) == 0 {
		return nil, errors.New("app.BuildCustomDBConnection, pass must be not empty")
	}

	if len(dbName) == 0 {
		return nil, errors.New("app.BuildCustomDBConnection, dbName must be not empty")
	}

	dsn := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		host, port, user, pass, dbName)

	db, err := sqlx.Connect("postgres", dsn)

	if err != nil {
		return nil, errors.Wrap(err, "app.BuildCustomDBConnection, unable to connect to DB")
	}

	if err := db.Ping(); err != nil {
		return nil, errors.Wrap(err, "app.BuildCustomDBConnection, unable to ping DB")
	}

	return db, nil
}

func (app *app) initDBConnection() (*sqlx.DB, error) {
	app.Config().SetDefault("UNIM_DB_PORT", 5432)
	host := app.Config().GetString("UNIM_DB_HOST")
	user := app.Config().GetString("UNIM_DB_USER")
	port := app.Config().GetString("UNIM_DB_PORT")
	pass := app.Config().GetString("UNIM_DB_PASS")
	dbName := app.Config().GetString("UNIM_DB_DB")

	if len(host) == 0 || len(user) == 0 || len(pass) == 0 || len(dbName) == 0 {
		return nil, nil
	}

	dsn := fmt.Sprintf("host=%s port=%s user=%s "+
		"password=%s dbname=%s sslmode=disable",
		host, port, user, pass, dbName)

	db, err := sqlx.Connect("postgres", dsn)

	if err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("app.initDBConnection, unable to connect to db by dsn: %s", dsn))
	}

	return db, nil
}

func (app *app) initRedisPool() (*redis.Pool, error) {
	redisHost := app.Config().GetString("UNIM_REDIS_HOST")
	redisPort := app.Config().GetString("UNIM_REDIS_PORT")
	redisPassword := app.Config().GetString("UNIM_REDIS_PASSWORD")

	dialOptions := make([]redis.DialOption, 0)
	if len(redisHost) == 0 || len(redisPort) == 0 {
		return nil, nil
	}

	if len(redisPassword) > 0 {
		dialOptions = append(dialOptions, redis.DialPassword(redisPassword))
	}

	address := fmt.Sprintf("%s:%s", redisHost, redisPort)

	return &redis.Pool{
		MaxIdle:     3,
		IdleTimeout: 240 * time.Second,
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", address, dialOptions...)
			if err != nil {
				return nil, errors.Wrap(err, fmt.Sprintf("app.initRedisPool, unable to dial to redis: %s", address))
			}
			return c, err
		},

		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			_, err := c.Do("PING")
			return err
		},
	}, nil
}

func (app *app) initLogger() (*zap.Logger, error) {
	outputPaths := make([]string, 0)
	internalErrorsOutputPaths := []string{"stderr"}

	app.Config().SetDefault("UNIM_LOGS_LEVEL", "debug")
	logsLevelStr := app.Config().GetString("UNIM_LOGS_LEVEL")
	logsLevel := new(zapcore.Level)

	err := logsLevel.Set(logsLevelStr)

	if err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("app.initLogger, unable to send log level: %s", logsLevelStr))
	}

	logsFile := app.Config().GetString("UNIM_LOGS_FILE")

	if len(logsFile) > 0 {
		internalErrorsOutputPaths = append(internalErrorsOutputPaths, logsFile)
		outputPaths = append(outputPaths, logsFile)
	}

	app.Config().SetDefault("UNIM_LOGS_STDOUT", "true")
	logsStdOut := app.Config().GetString("UNIM_LOGS_STDOUT")

	if logsStdOut == "true" {
		internalErrorsOutputPaths = append(internalErrorsOutputPaths, "stdout")
		outputPaths = append(outputPaths, "stdout")
	}

	cores := make([]zapcore.Core, 0)
	cfg := zap.Config{
		Level:       zap.NewAtomicLevelAt(*logsLevel),
		Development: false,
		Encoding:    "console",
		EncoderConfig: zapcore.EncoderConfig{
			TimeKey:        "d",
			LevelKey:       "l",
			NameKey:        "n",
			CallerKey:      "c",
			MessageKey:     "m",
			StacktraceKey:  "s",
			LineEnding:     zapcore.DefaultLineEnding,
			EncodeLevel:    zapcore.LowercaseLevelEncoder,
			EncodeTime:     zapcore.ISO8601TimeEncoder,
			EncodeCaller:   zapcore.FullCallerEncoder,
			EncodeDuration: zapcore.SecondsDurationEncoder,
		},
		OutputPaths:      outputPaths,
		ErrorOutputPaths: internalErrorsOutputPaths,
	}

	defaultLogger, err := cfg.Build()

	if err != nil {
		return nil, errors.Wrap(err, "app.initLogger, unable to build default logger")
	}

	cores = append(cores, defaultLogger.Core())

	sentryURL := app.Config().GetString("UNIM_SENTRY_URL")
	hasSentryURL := len(sentryURL) > 0
	if hasSentryURL {
		minimalLogLevel := zapcore.Level(zapcore.WarnLevel)
		zapSentry := zapsentry.Configuration{DSN: sentryURL, MinimalLogLevel: &minimalLogLevel}

		zapSentryCore, err := zapSentry.Build()

		if err != nil {
			return nil, errors.Wrap(err, "app.initLogger, unable to build zap sentry logger")
		}

		cores = append(cores, zapSentryCore)
	}

	logger := zap.New(zapcore.NewTee(cores...))

	zap.ReplaceGlobals(logger)

	return logger, nil
}

func (app *app) initMetrics() error {
	constLabels := map[string]string{
		"deployEnv":   app.deployInfo.DeployEnv,
		"appName":     app.deployInfo.AppName,
		"appInstance": app.deployInfo.AppInstance,
		"appVersion":  ApplicationVersion,
	}

	prometheus.DefaultRegisterer = prometheus.WrapRegistererWith(constLabels, prometheus.DefaultRegisterer)

	return nil
}

func (app *app) BuildServiceLogger(service string) (*zap.Logger, error) {
	serivceLogFile := app.Config().GetString("UNIM_SERVICE_LOGS_FILE")

	internalErrorsOutputPaths := []string{"stdout", "stderr"}
	outputPaths := []string{"stdout"}

	if len(serivceLogFile) > 0 {
		internalErrorsOutputPaths = append(internalErrorsOutputPaths, serivceLogFile)
		outputPaths = append(outputPaths, serivceLogFile)
	}

	cfg := zap.Config{
		Level:       zap.NewAtomicLevelAt(zap.DebugLevel),
		Development: false,
		Encoding:    "json",
		EncoderConfig: zapcore.EncoderConfig{
			TimeKey:        "ts",
			LevelKey:       "level",
			NameKey:        "n",
			CallerKey:      "c",
			MessageKey:     "msg",
			StacktraceKey:  "s",
			LineEnding:     zapcore.DefaultLineEnding,
			EncodeLevel:    zapcore.LowercaseLevelEncoder,
			EncodeTime:     zapcore.ISO8601TimeEncoder,
			EncodeCaller:   zapcore.FullCallerEncoder,
			EncodeDuration: zapcore.SecondsDurationEncoder,
		},
		OutputPaths:      outputPaths,
		ErrorOutputPaths: internalErrorsOutputPaths,
	}

	defaultLogger, err := cfg.Build()

	if err != nil {
		return nil, errors.Wrap(err, "app.BuildServiceLogger, unable to build service logger")
	}

	core := defaultLogger.Core().With([]zapcore.Field{zap.String("service", service)})
	logger := zap.New(core)

	return logger, nil
}

func (app *app) InitAmqpConnection() (*amqp.Connection, error) {
	host := app.Config().GetString("UNIM_AMQP_HOST")

	if len(host) == 0 {
		return nil, nil
	}

	amqpURL, err := amqpHelpers.BuildAMQPConnectionUrl(app.Config().GetString("UNIM_AMQP_HOST"),
		app.Config().GetInt("UNIM_AMQP_PORT"),
		app.Config().GetString("UNIM_AMQP_USER"),
		app.Config().GetString("UNIM_AMQP_PASS"),
		app.Config().GetString("UNIM_AMQP_VHOST"))

	if err != nil {
		return nil, errors.Wrap(err, "Application.InitAmqpConnection, unable to build AMQP connection url")
	}

	connection, err := amqp.Dial(amqpURL)

	if err != nil {
		return nil, errors.Wrap(err, "Application.InitAmqpConnection, unable to get amqp connection")
	}

	return connection, nil
}

var instanceApp App

func NewApplication(name string, defaultConfig io.Reader) App {
	if instanceApp == nil {
		localBus, err := localbus.NewBus()
		if err != nil {
			panic(err)
		}
		newApp := &app{
			name:          name,
			bus:           localBus,
			defaultConfig: defaultConfig,
		}
		instanceApp = newApp
	}

	return instanceApp
}

//Application get initialized application or initialize and return it
func Application() App {
	if instanceApp == nil {
		panic("App not initialized yet")
	}

	return instanceApp
}

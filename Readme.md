# Intention
Common tools for work with go projects
# Dependencies
libmagic

# Start working
* get dependencies - `go get`
* verify - `go test bitbucket.org/unimhq/core/pkg`

# Drone build
* [get auth token](https://unimhq.atlassian.net/wiki/spaces/TECH/pages/587956225/Continuous+Integration+drone.io])
* Create cache directories.
    ```
     GOLANG_VERSION=1.12.6 # keep in sync with go in `go-builder` image
     mkdir -p /tmp/unim/go/$GOLANG_VERSION/mod-cache
     mkdir -p /tmp/unim/go/$GOLANG_VERSION/build-cache
    ```
* drone exec --trusted --netrc-machine=bitbucket.org --netrc-username=x-token-auth --netrc-password=TOKEN
